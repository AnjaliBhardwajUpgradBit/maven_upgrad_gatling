package forumapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetV1Answers {
  val get_v1_answers = feed(forumQuestionAnswers).exec(http("User gets answers via v1 api")
    .get("/v1/answers/?questionId=${question_id_forum}")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}", "courseId" -> "${course_id_forum}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of /v1/answers/ api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
