package forumapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object QuestionForum {
  val question_forum = exec(http("User gets bulk quiz")
    .get("/v1/users/272685/questions")
    .headers(headers_staging_forum)
    .check(status.is(200)))
}
//To print the api response
//    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
//    .exec( session => {
//               println( "Result of signup api:" )
//               println( session( "RESPONSE_DATA" ).as[String] )
//               session})
//}
