package forumapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object PostV1Answers {
  val post_v1_answers = feed(forumQuestionAnswers).exec(http("User post v1 answers")
    .post("/v1/answers/")
    .headers(Map("Content-Type" -> "application/json", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}", "courseId" -> "${course_id_forum}"))
    .body(StringBody("""{"questionId": ${question_id_forum} , "body": "this is not a test answer"}""")).asJson
    .check(status.in(201))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of /v1/answers/ api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
