package forumapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetV1Contexts {
  val get_v1_contexts = feed(paidActiveUser).exec(http("User gets v1 contexts")
    .get("/v1/contexts/")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}", "courseId" -> "${course_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of /v1/contexts/ api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
