package careerapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetCohortV1 {
  val get_cohort_v1 = feed(paidActiveUser).exec(http("User gets cohort v1")
    .get("/v1/cohort")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}", "course_id" -> "${course_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check(jsonPath("$").saveAs("RESPONSE_DATA")))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
       exec( session => {
                      println( "Error logs of v1/cohort api:" )
                      println( session( "RESPONSE_DATA" ).as[String] )
                      session})
       }
}
