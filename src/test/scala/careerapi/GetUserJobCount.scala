package careerapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetUserJobCount {
  val get_user_job_count = feed(paidActiveUser).exec(http("User gets job count")
    .get("/v2/user/job")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}", "course_id" -> "${course_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check(jsonPath("$").saveAs("RESPONSE_DATA")))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
       exec( session => {
                      println( "Error logs of /v2/user/job api:" )
                      println( session( "RESPONSE_DATA" ).as[String] )
                      session})
       }
}
