package assessmentapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object QuizBulk {
  val quiz_bulk = feed(paidActiveUser).exec(http("User gets bulk quiz")
    .get("/v2/quiz_sessions/bulk/?ids=${quiz_id}&get_definition=true")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}", "courseId" -> "${course_id}" ))
    .check(status.is(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "400" || session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of /v2/quiz_sessions/bulk/?ids=${quiz_id} api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
