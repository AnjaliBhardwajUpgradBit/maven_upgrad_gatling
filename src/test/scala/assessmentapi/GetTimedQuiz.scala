package assessmentapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetTimedQuiz {
  val get_timed_quiz = exec(http("User submits a quiz")
    .get("/v2/timed_quizsession/")
    .headers(headers_staging_central)
    .check(status.is(200)))
}
