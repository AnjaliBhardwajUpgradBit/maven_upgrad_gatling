package assessmentapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GradesEvaluation {
  val grade_evaluation = feed(paidActiveUser).exec(http("User evaluates the grade")
    .get("/v1/grades/evaluations/?questionId=${question_id}&courseId=${course_id}&toBeSubmitted=false")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}", "courseId" -> "${course_id}" ))
    .check(status.is(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "400" || session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of /v1/grades/evaluations/?questionId= api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
