package assessmentapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object StartQuiz {
  val start_quiz = exec(http("User starts quiz")
    .post("/v2/quizsession/6127712/start/")
    .headers(headers_staging_central)
    .check(status.is(200)))
}
