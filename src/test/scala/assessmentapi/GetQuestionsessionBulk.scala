package assessmentapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetQuestionsessionBulk {
  val get_questionsession_bulk = feed(paidActiveUser).exec(http("User gets bulk question session")
    .get("/v2/question_sessions/bulk/?ids=${question_id},${question_id_2},${question_id_3}&get_definition=true")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}", "courseId" -> "${course_id}" ))
    .check(status.is(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "400" || session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of /v2/question_sessions/bulk/ api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
