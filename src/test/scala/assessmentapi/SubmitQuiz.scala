package assessmentapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object SubmitQuiz {
  val submit_quiz = exec(http("User submits a quiz")
    .get("/v2/quizsession/6127677/submit/")
    .headers(headers_staging_central)
    .check(status.is(200)))
}
