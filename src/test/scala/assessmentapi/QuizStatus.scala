package assessmentapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object QuizStatus {
  val quiz_status = feed(paidActiveUser).exec(http("User gets quiz status")
    .get("/v1/testsessions/admin/user_quiz_status/?quizIds=${quiz_id}")
    //.headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}", "courseId" -> "${course_id}" ))
    .headers(headers_mercury_assessment_app_token)
    .check(status.is(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "400" || session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of /v1/testsessions/admin/user_quiz_status api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
