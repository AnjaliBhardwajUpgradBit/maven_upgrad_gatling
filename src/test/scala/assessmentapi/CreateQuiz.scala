package assessmentapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object CreateQuiz {
  val create_quiz = exec(http("user creates quiz")
    .post("/v2/quizzes/")
    .headers(headers_staging_assessment)
    .body(ElFileBody(create_new_quiz)).asJson
    .check(status.is(200)))
}
