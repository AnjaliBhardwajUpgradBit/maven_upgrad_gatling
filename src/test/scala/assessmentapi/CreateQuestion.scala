package assessmentapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object CreateQuestion {
  val create_question = exec(http("User creates question")
    .post("/v2/questions/")
    .headers(headers_staging_assessment)
    .body(ElFileBody(create_new_question)).asJson
    .check(status.is(200)))
}
