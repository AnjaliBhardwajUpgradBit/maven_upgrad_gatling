package assessmentapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GradedSubmissionModule {
  val graded_submission_module = feed(paidActiveUser).exec(http("User gets graded submission module")
    .get("/v1/grades/graded_submission_modules_in_course/${course_id}/")
    //.headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}", "courseId" -> "${course_id}" ))
    .headers(headers_mercury_assessment_app_token)
    .check(status.is(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "400" || session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of /v1/grades/graded_submission_modules_in_course/ api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
