package runner.assessment

import assessmentapi.CreateQuestion._
import assessmentapi.CreateQuiz._
import assessmentapi.GetTimedQuiz._
import assessmentapi.QuizBulk._
import io.gatling.core.Predef._
import loadapi.Base._

class AssessmentLifeCycleRunner extends Simulation {
  val create_quiz_run = scenario("User creates quiz").exec(create_quiz)
  val create_question_run = scenario("User creates question").exec(create_question)
  val get_quiz_run = scenario("User gets quiz").exec(quiz_bulk)
  val get_timed_quiz_run = scenario("User gets timed quiz").exec(get_timed_quiz)
  setUp(
    create_quiz_run.inject(
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol_assessment),
    create_question_run.inject(
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol_assessment),
    get_quiz_run.inject(
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol_assessment),
    get_timed_quiz_run.inject(
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol_assessment),

  )
}
