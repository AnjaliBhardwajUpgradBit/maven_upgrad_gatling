package runner.assessment

import assessmentapi.GetQuestionsessionBulk._
import assessmentapi.GradedSubmissionModule._
import assessmentapi.GradesEvaluation._
import assessmentapi.QuizBulk._
import assessmentapi.QuizStatus._
import assessmentapi.TimedQuizsession._
import io.gatling.core.Predef._
import loadapi.Base._

class AssessmentAllRunner extends Simulation {

  val usersC = users.toInt
  val rampQuizsessionbulk = rampUpUsers.toInt
  val rampQuizsessionbulkV2 = (rampQuizsessionbulk*1.20).toInt

  val rampTimedquizsession = (rampQuizsessionbulkV2*0.219).toInt
  val rampQuestionsessionbulk = (rampQuizsessionbulkV2*0.268).toInt
  val rampAdmintestsession = (rampQuizsessionbulkV2*0.032).toInt
  val rampGradesEvaluation = (rampQuizsessionbulkV2*0.011).toInt
  val rampGradedsubmission = (rampQuizsessionbulkV2*0.007).toInt
  val rampAssignmentforGrading = (rampQuizsessionbulkV2*0.006).toInt

  //val create_quiz_run = scenario("User creates quiz").exec(create_quiz)
  //val create_question_run = scenario("User creates question").exec(create_question)
  //val get_assignment_grading_run = scenario("User gets assignment for grading").exec(assignment_grading)
  //val get_timed_quiz_run = scenario("User gets timed quiz").exec(get_timed_quiz)

  val get_quiz_run = scenario("User gets quiz").exec(quiz_bulk)
  val get_questionsession_bulk_run = scenario("User gets question session bulk").exec(get_questionsession_bulk)
  val graded_submission_module_run = scenario("User gets submission module").exec(graded_submission_module)
  val grade_evaluation_run = scenario("User gets grade evaluation").exec(grade_evaluation)
  val quiz_status_run = scenario("User gets quiz status").exec(quiz_status)
  val timed_quizssesion_run = scenario("User gets timed quizsessions").exec(timed_quizsesion)

  setUp(
    get_quiz_run.inject(
      rampUsers(rampQuizsessionbulkV2) during (rampUpTime seconds)
    ).protocols(httpProtocol_assessment_mercury),
    get_questionsession_bulk_run.inject(
      rampUsers(rampQuestionsessionbulk) during (rampUpTime seconds)
    ).protocols(httpProtocol_assessment_mercury),
    graded_submission_module_run.inject(
      rampUsers(rampGradedsubmission) during (rampUpTime seconds)
    ).protocols(httpProtocol_assessment_mercury),
    grade_evaluation_run.inject(
      rampUsers(rampGradesEvaluation) during (rampUpTime seconds)
    ).protocols(httpProtocol_assessment_mercury),
    quiz_status_run.inject(
      rampUsers(rampAdmintestsession) during (rampUpTime seconds)
    ).protocols(httpProtocol_assessment_mercury),
    timed_quizssesion_run.inject(
      rampUsers(rampTimedquizsession) during (rampUpTime seconds)
    ).protocols(httpProtocol_assessment_mercury)
  )
}
