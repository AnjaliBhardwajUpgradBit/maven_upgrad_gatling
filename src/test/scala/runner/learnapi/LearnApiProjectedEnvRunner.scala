package runner.learnapi

import centralapi.learnapi.GetAchievementStat._
import centralapi.learnapi.GetAvailableCatalog._
import centralapi.learnapi.GetCourseProgress._
import centralapi.learnapi.GetCurrentProgress._
import centralapi.learnapi.GetEnrollment._
import centralapi.learnapi.GetLmsConfig._
import centralapi.learnapi.GetModuleProgress._
import centralapi.learnapi.GetSegmentProgress._
import centralapi.learnapi.GetUserSettings._
import centralapi.learnapi.ModuleGroup._
import centralapi.learnapi.ModuleGroupById._
import centralapi.learnapi.RecentLogin._
import centralapi.learnapi.SkippedComponent._
import io.gatling.core.Predef._
import loadapi.Base._


class LearnApiProjectedEnvRunner extends Simulation {

  val usersC = users.toInt
  val rampUpUserCurrentProgressS = rampUpUsers.toInt
  val rampUpUserCurrentProgress = (rampUpUserCurrentProgressS*1.20).toInt
  // Multiplier factor is expected DAU by max dau (15310/12758)
  val rampUpUsersC = (rampUpUserCurrentProgress*0.322*3.73).toInt
  // Multiplier factor is expected DAU by max dau (230000/61651)
  val rampUpRL = (rampUpUserCurrentProgress*0.565).toInt
  val rampUpSkipped = (rampUpUserCurrentProgress*0.566).toInt
  val rampUpLMS = (rampUpUserCurrentProgress*0.41).toInt
  val rampUpUserSettings = (rampUpUserCurrentProgress*0.036).toInt
  val rampUpCourseProgress = (rampUpUserCurrentProgress*0.81).toInt
  val rampUpModuleProgress = (rampUpUserCurrentProgress*0.41).toInt
  val rampUpSegmentProgress = (rampUpUserCurrentProgress*0.3).toInt
  val rampUpAvailableCatalog = (rampUpUserCurrentProgress*0.084).toInt
  val rampUpModulegroup = (rampUpUserCurrentProgress*0.009).toInt
  val rampUpModulegroupById = (rampUpUserCurrentProgress*0.093).toInt
  val rampUpGetEnrollment = (rampUpUserCurrentProgress*0.097).toInt
  val rampUpAchievement = (rampUpUserCurrentProgress*0.011).toInt

  val recent_login_cycle = scenario("User checks recent login").exec(recent_login)
  val skipped_component_cycle = scenario("User checks skipped components").exec(skipped_component)
  val get_lms_config_cycle = scenario("User checks his sound and video config").exec(get_lms_config)
  val get_user_settings_cycle = scenario("User checks permissions").exec(get_user_settings)
  val get_course_progress_cycle = scenario("User checks course progress").exec(get_course_progress)
  val get_current_progress_cycle = scenario("User checks segment page and current progress").exec(get_segment_page, get_current_progress)
  val get_module_progress_cycle = scenario("User checks module progress").exec(get_module_progress)
  val get_segment_progress_cycle = scenario("User checks segment progress").exec(get_segment_progress)
  val get_available_catalog_cycle = scenario("User checks available catalog").exec(get_available_catalog)
  val modulegroup_cycle = scenario("User checks all module groups").exec(modulegroup)
  val modulegroup_byid_cycle = scenario("User checks module group by id").exec(modulegroup_byid)
  val get_enrollment_cycle = scenario("User checks enrollments in courses").exec(get_enrollment)
  val get_achievement_stat_cycle = scenario("User checks the badges").exec(get_achievement_stat)

  setUp(
    recent_login_cycle.inject(
      rampUsers(rampUpRL) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    skipped_component_cycle.inject(
      rampUsers(rampUpSkipped) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    get_lms_config_cycle.inject(
      rampUsers(rampUpLMS) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    get_user_settings_cycle.inject(
      rampUsers(rampUpUserSettings) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    get_course_progress_cycle.inject(
      rampUsers(rampUpCourseProgress) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    get_current_progress_cycle.inject(
      rampUsers(rampUpUserCurrentProgress) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    get_module_progress_cycle.inject(
      rampUsers(rampUpModuleProgress) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    get_segment_progress_cycle.inject(
      rampUsers(rampUpSegmentProgress) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    get_available_catalog_cycle.inject(
      rampUsers(rampUpAvailableCatalog) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    modulegroup_cycle.inject(
      rampUsers(rampUpModulegroup) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    modulegroup_byid_cycle.inject(
      rampUsers(rampUpModulegroupById) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    get_enrollment_cycle.inject(
      rampUsers(rampUpGetEnrollment) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    get_achievement_stat_cycle.inject(
      rampUsers(rampUpAchievement) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury))
}
