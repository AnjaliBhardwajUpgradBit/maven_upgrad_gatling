package runner.learnapi

import centralapi.learnapi.ElectiveConfiguration._
import io.gatling.core.Predef._
import loadapi.Base._

class ElectiveRunner extends Simulation {

  val elective_lifecycle = scenario("User checks electives").exec(elective_configuration)
  setUp(
    elective_lifecycle.inject(
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury))
}
