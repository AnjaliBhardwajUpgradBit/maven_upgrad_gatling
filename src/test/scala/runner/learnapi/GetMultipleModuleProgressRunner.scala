package runner.learnapi

import centralapi.learnapi.GetMultipleModuleProgress._
import io.gatling.core.Predef._
import loadapi.Base._

class GetMultipleModuleProgressRunner extends Simulation {
  val multiple_module_progress_cycle = scenario("User checks progress").exec(get_multiple_module_progress)
  setUp(
    multiple_module_progress_cycle.inject(
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury))
}
