package runner

import centralapi.authapi.CheckEmailExists._
import centralapi.growthapi.GetApplication._
import centralapi.growthapi.Lead._
import centralapi.growthapi.OneTimeToken._
import io.gatling.core.Predef._
import loadapi.Base._


class FailedApiRunner extends Simulation {

val failed_cycle = scenario("Check failed API's").exec(get_application, lead, check_email_exists, one_time_token_generate, one_time_token_verify)

setUp(
  failed_cycle.inject(
    rampUsers(rampUpUsers) during (rampUpTime seconds)
  ).protocols(httpProtocol))
}
