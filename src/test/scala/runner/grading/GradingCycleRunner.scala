package runner.grading

import gradingapi.CurrentPenalty._
import gradingapi.GpaUploadStatus._
import gradingapi.GradeConfig._
import gradingapi.MarkAsSeen._
import gradingapi.PenaltyOverride._
import gradingapi.QuizModuleListing._
import gradingapi.QuizModuleListingCourse._
import gradingapi.ScoreForUser._
import gradingapi.ScoreUploadStatus._
import gradingapi.UserPerformance._
import io.gatling.core.Predef._
import loadapi.Base._

class GradingCycleRunner extends Simulation {
  val usersC = users.toInt
  val rampGradeConfig = rampUpUsers.toInt

  val rampPenaltyOverride = (rampGradeConfig*0.855).toInt
  val rampUserPerformanceCourse = (rampGradeConfig*0.03).toInt
  //val rampMarkscoreasseen = (rampGradeConfig*0.007).toInt
  val rampGetscoreofauserforquizId = (rampGradeConfig*0.006).toInt
  val rampGetquizmodulelistingforamoduleinacourse = (rampGradeConfig*0.003).toInt
  val rampGetcurrentpenalty = (rampGradeConfig*0.001).toInt
  val rampGetgpaUploadStatusforcourse = (rampGradeConfig*0.002).toInt
  val rampGetScoreUploadStatus = (rampGradeConfig*0.001).toInt
  val rampGetquizmodulelistingforacourse = (rampGradeConfig*0.002).toInt

  val get_grade_config = scenario("User gets grade config").exec(grade_config)
  val get_penalty_overide = scenario("User gets penalty override").exec(score_for_user)
  val get_user_performance_course = scenario("User ").exec(pently_override)
  val get_score_user_quiz = scenario("User marks as seen").exec(user_performance)
  val get_marks_seen = scenario("User gets score for a quiz").exec(mark_as_seen)
  val get_quiz_module_listing = scenario("Usergets quiz module listing ").exec(quiz_module_listing)
  val get_current_penalty = scenario("User gets current penalty").exec(current_penalty)
  val get_gpa_upload_status = scenario("User gets gpa upload status").exec(gpa_upload_status)
  val get_score_upload_status = scenario("User gets score upload status").exec(score_upload_status)
  val get_quiz_module_listing_course = scenario("User gets quiz module listing of course").exec(quiz_module_listing_course)

  setUp(
    get_grade_config.inject(
      rampUsers(rampGradeConfig) during (rampUpTime seconds)
    ).protocols(httpProtocol_grading_mercury),

    get_penalty_overide.inject(
      rampUsers(rampPenaltyOverride) during (rampUpTime seconds)
    ).protocols(httpProtocol_grading_mercury),

    get_user_performance_course.inject(
      rampUsers(rampUserPerformanceCourse) during (rampUpTime seconds)
    ).protocols(httpProtocol_grading_mercury),

    //get_marks_seen.inject(
    //  rampUsers(rampMarkscoreasseen) during (rampUpTime seconds)
    //).protocols(httpProtocol_grading_mercury),

    get_score_user_quiz.inject(
      rampUsers(rampGetscoreofauserforquizId) during (rampUpTime seconds)
    ).protocols(httpProtocol_grading_mercury),

    get_quiz_module_listing.inject(
      rampUsers(rampGetquizmodulelistingforamoduleinacourse) during (rampUpTime seconds)
    ).protocols(httpProtocol_grading_mercury),

    get_current_penalty.inject(
      rampUsers(rampGetcurrentpenalty) during (rampUpTime seconds)
    ).protocols(httpProtocol_grading_mercury),

    get_gpa_upload_status.inject(
      rampUsers(rampGetgpaUploadStatusforcourse) during (rampUpTime seconds)
    ).protocols(httpProtocol_grading_mercury),

    get_score_upload_status.inject(
      rampUsers(rampGetScoreUploadStatus) during (rampUpTime seconds)
    ).protocols(httpProtocol_grading_mercury),

    get_quiz_module_listing_course.inject(
      rampUsers(rampGetquizmodulelistingforacourse) during (rampUpTime seconds)
    ).protocols(httpProtocol_grading_mercury)
  )
}
