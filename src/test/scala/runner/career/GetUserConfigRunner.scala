package runner.career

import careerapi.GetUserConfig._
import io.gatling.core.Predef._
import loadapi.Base._


class GetUserConfigRunner extends Simulation {

  val usersC = users.toInt
  val rampUserProfileV2 = rampUpUsers.toInt

val get_user_config_cycle = scenario("User gets the config").exec(get_user_config)

  setUp(
    get_user_config_cycle.inject(
      rampUsers(rampUserProfileV2) during (rampUpTime seconds)
    ).protocols(httpProtocol_career_mercury)
  )
}
