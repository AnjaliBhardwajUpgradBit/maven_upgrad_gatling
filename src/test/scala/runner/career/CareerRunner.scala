package runner.career

import careerapi.GetCohortV1._
import careerapi.GetCohortV2._
import careerapi.GetScore._
import careerapi.GetTransitions._
import careerapi.GetUserConfig._
import careerapi.GetUserJobCount._
import careerapi.UserProfileV1._
import careerapi.UserProfileV2._
import io.gatling.core.Predef._
import loadapi.Base._


class CareerRunner extends Simulation {

  val usersC = users.toInt
  val rampUserProfileV2I = rampUpUsers.toInt
  val rampUserProfileV2 = (rampUserProfileV2I*1.20).toInt

  val rampCohortV2 = (rampUserProfileV2*0.084).toInt
  val rampCohortV1 = (rampUserProfileV2*0.018).toInt
  val rampUserProfileV1 = (rampUserProfileV2*0.002).toInt
  val rampUserConfig = (rampUserProfileV2*0.026).toInt
  val rampTransition = (rampUserProfileV2*0.014).toInt
  val rampUserJob = (rampUserProfileV2*0.009).toInt
  val rampScore = (rampUserProfileV2*0.025).toInt

  val user_profile_v2_cycle = scenario("User checks v2 profile").exec(user_profile_v2)
  val user_profile_v1_cycle = scenario("User checks v1 profile").exec(user_profile_v1)
  val get_cohort_v2_cycle = scenario("User checks v2 cohort").exec(get_cohort_v2)
  val get_cohort_v1_cycle = scenario("User checks checks v1 cohort").exec(get_cohort_v1)
  val get_transitions_cycle = scenario("User checks transition").exec(get_transitions)
  val get_user_job_count_cycle = scenario("User checks job count").exec(get_user_job_count)
  val get_user_score_cycle = scenario("User checks score").exec(get_user_score)
  val get_user_config_cycle = scenario("User gets the config").exec(get_user_config)

  setUp(
    user_profile_v2_cycle.inject(
      rampUsers(rampUserProfileV2) during (rampUpTime seconds)
    ).protocols(httpProtocol_career_mercury),
    user_profile_v1_cycle.inject(
      rampUsers(rampUserProfileV1) during (rampUpTime seconds)
    ).protocols(httpProtocol_career_mercury),
    get_cohort_v2_cycle.inject(
      rampUsers(rampCohortV2) during (rampUpTime seconds)
    ).protocols(httpProtocol_career_mercury),
    get_cohort_v1_cycle.inject(
      rampUsers(rampCohortV1) during (rampUpTime seconds)
    ).protocols(httpProtocol_career_mercury),
    get_transitions_cycle.inject(
      rampUsers(rampTransition) during (rampUpTime seconds)
    ).protocols(httpProtocol_career_mercury),
    get_user_job_count_cycle.inject(
      rampUsers(rampUserJob) during (rampUpTime seconds)
    ).protocols(httpProtocol_career_mercury),
    get_user_score_cycle.inject(
      rampUsers(rampScore) during (rampUpTime seconds)
    ).protocols(httpProtocol_career_mercury),
    get_user_config_cycle.inject(
      rampUsers(rampUserConfig) during (rampUpTime seconds)
    ).protocols(httpProtocol_career_mercury)
  )
}
