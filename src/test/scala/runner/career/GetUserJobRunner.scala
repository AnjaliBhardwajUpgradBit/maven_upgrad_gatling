package runner.career

import careerapi.GetUserJobCount._
import io.gatling.core.Predef._
import loadapi.Base._


class GetUserJobRunner extends Simulation {

  val usersC = users.toInt
  val rampUserProfileV2 = rampUpUsers.toInt

  val get_user_job_count_cycle = scenario("User checks job count").exec(get_user_job_count)

  setUp(
    get_user_job_count_cycle.inject(
      rampUsers(rampUserProfileV2) during (rampUpTime seconds)
    ).protocols(httpProtocol_career_mercury)
  )
}
