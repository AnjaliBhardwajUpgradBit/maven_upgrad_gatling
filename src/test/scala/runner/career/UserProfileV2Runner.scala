package runner.career

import careerapi.UserProfileV2._
import io.gatling.core.Predef._
import loadapi.Base._


class UserProfileV2Runner extends Simulation {

  val usersC = users.toInt
  val rampUserProfileV2 = rampUpUsers.toInt

  val user_profile_v2_cycle = scenario("User checks v2 profile").exec(user_profile_v2)

  setUp(
    user_profile_v2_cycle.inject(
      rampUsers(rampUserProfileV2) during (rampUpTime seconds)
    ).protocols(httpProtocol_career_mercury)
  )
}
