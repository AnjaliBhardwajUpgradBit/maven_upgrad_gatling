package runner.cloudfront

import cloudfrontapi.DegreePage._
import cloudfrontapi.Home._
import io.gatling.core.Predef._
import loadapi.Base._

class CloudfrontRunner extends Simulation {

  val cloudfront_cycle = scenario("User calls cloudfront API's").exec(home, degree_page)
  setUp(
    cloudfront_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol_cloudfront_mercury))
}
