package runner.central.growth

import centralapi.authapi.Authenticate._
import centralapi.authapi.Signup._
import centralapi.authapi.SignupApply._
import centralapi.growthapi.Application._
import centralapi.growthapi.CampaignContact._
import centralapi.growthapi.DropLead._
import centralapi.growthapi.EmiPartner._
import centralapi.growthapi.GetApplication._
import centralapi.growthapi.Lead._
import io.gatling.core.Predef._
import loadapi.Base._

class EnvReadinessRunnerPerSecondRunner extends Simulation {

  val rampUpUsersC = rampUpUsers.toDouble
  val rampUpEmi = (rampUpUsersC*0.02).toDouble
  val rampUpApplication = (rampUpUsersC*0.064).toDouble
  val rampUpdroplead = (rampUpUsersC*0.048).toDouble
  val rampUpSignup = (rampUpUsersC*0.025).toDouble
  val rampUpignupApply = (rampUpUsersC*0.00022).toDouble
  val rampUpCreateApplication = (rampUpUsersC*0.0073).toDouble

  val campaign_contact_cycle = scenario("User checks campaign").exec(campaign_contact)
  val emi_partner_cycle = scenario("User emi partner").exec(emi_partner)
  val application_cycle = scenario("User gets application").exec(get_application)
  val drop_lead_cycle = scenario("User drops the lead").exec(drop_lead)
  val signup_cycle = scenario("User signs up").exec(signup)
  val signup_and_apply_cycle = scenario("User signsup and apply").exec(signup_and_apply)
  val create_application_cycle = scenario("User applies to program").exec(application)
  val authenticate_cycle = scenario("Usergets authenticated").exec(authenticate)
  val check_lead_cycle = scenario("User checks lead").exec(lead)
  setUp(
    campaign_contact_cycle.inject(
      constantUsersPerSec(rampUpUsersC) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),
    emi_partner_cycle.inject(
      constantUsersPerSec(rampUpEmi) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),
    application_cycle.inject(
      constantUsersPerSec(rampUpApplication) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),
    drop_lead_cycle.inject(
      constantUsersPerSec(rampUpdroplead) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),
    signup_cycle.inject(
      constantUsersPerSec(rampUpSignup) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),
    signup_and_apply_cycle.inject(
      constantUsersPerSec(rampUpignupApply) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),
    create_application_cycle.inject(
      constantUsersPerSec(rampUpCreateApplication) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),
    authenticate_cycle.inject(
      constantUsersPerSec(rampUpSignup) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),
    check_lead_cycle.inject(
      constantUsersPerSec(rampUpSignup) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol)
  )
}
