package runner.central.growth

import centralapi.authapi.Signup._
import io.gatling.core.Predef._
import loadapi.Base._

class SignupRunner extends Simulation {

  val signup_cycle = scenario("User signs up").exec(signup)
  setUp(
    signup_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
