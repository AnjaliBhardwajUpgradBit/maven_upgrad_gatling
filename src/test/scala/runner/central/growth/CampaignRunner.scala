package runner.central.growth

import centralapi.growthapi.CampaignContact._
import io.gatling.core.Predef._
import loadapi.Base._

class CampaignRunner extends Simulation {
  val signup_cycle = scenario("User checks campaign number").exec(campaign_contact)
  setUp(
    signup_cycle.inject(
      //atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    //).protocols(httpProtocol))
    ).protocols(httpProtocol))
}
