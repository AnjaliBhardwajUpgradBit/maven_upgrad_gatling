package runner.central.growth

import centralapi.growthapi.GetApplication._
import io.gatling.core.Predef._
import loadapi.Base._

class GetApplicationRunner extends Simulation {

  val campaign_contact_cycle = scenario("User runs Sceanrio 3").exec(get_application)
  setUp(
    campaign_contact_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
