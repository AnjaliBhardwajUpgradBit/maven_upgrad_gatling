package runner.central.growth
import centralapi.authapi.Authenticate._
import centralapi.authapi.CheckEmailExists._
import centralapi.authapi.LoginV2._
import centralapi.authapi.LoginV3._
import centralapi.authapi.PpandTc._
import centralapi.authapi.Signup._
import centralapi.authapi.SignupApply._
import centralapi.growthapi.Application._
import centralapi.growthapi.CampaignContact._
import centralapi.growthapi.DropLead._
import centralapi.growthapi.EmiPartner._
import centralapi.growthapi.GetAllBatches._
import centralapi.growthapi.GetApplication._
import centralapi.growthapi.GetBatchByPP._
import centralapi.growthapi.Lead._
import centralapi.growthapi.OneTimeToken._
import centralapi.growthapi.UpdateUserInfo._
import centralapi.growthapi.UserInfo._
import io.gatling.core.Predef._
import loadapi.Base._

class CentralGrowthRunner extends Simulation {

  val usersC = users.toInt
  val rampUpUserCurrentProgressS = rampUpUsers.toInt
  val rampUpUserCurrentProgress = (rampUpUserCurrentProgressS*1.20).toInt
  // Multiplier factor is expected DAU by max dau (15310/12758)
  val rampUpUsersC = (rampUpUserCurrentProgress*0.322*3.73).toInt
  // Multiplier factor is expected DAU by max dau (230000/61651)

  val rampUpPP = (rampUpUserCurrentProgress*0.099).toInt
  //reduced the frequency by 1/100 of RL
  val rampUpRL = (rampUpUserCurrentProgress*0.565).toInt

  val rampUpSkipped = (rampUpUserCurrentProgress*0.566).toInt
  val rampUpLMS = (rampUpUserCurrentProgress*0.41).toInt
  val rampUpUserSettings = (rampUpUserCurrentProgress*0.036).toInt
  val rampUpCourseProgress = (rampUpUserCurrentProgress*0.81).toInt
  val rampUpModuleProgress = (rampUpUserCurrentProgress*0.41).toInt
  val rampUpSegmentProgress = (rampUpUserCurrentProgress*0.3).toInt
  val rampUpAvailableCatalog = (rampUpUserCurrentProgress*0.084).toInt
  val rampUpModulegroup = (rampUpUserCurrentProgress*0.009).toInt
  val rampUpModulegroupById = (rampUpUserCurrentProgress*0.093).toInt
  val rampUpGetEnrollment = (rampUpUserCurrentProgress*0.097).toInt
  val rampUpAchievement = (rampUpUserCurrentProgress*0.011).toInt


  val rampUpEmi = (rampUpUsersC*0.02).toInt
  val rampUpApplication = (rampUpUsersC*0.064).toInt
  val rampUpdroplead = (rampUpUsersC*0.048).toInt
  val rampUpSignup = (rampUpUsersC*0.025).toInt
  val rampUpignupApply = (rampUpUsersC*0.00022).toInt
  val rampUpCreateApplication = (rampUpUsersC*0.0073).toInt

  val pp_and_tc_cycle = scenario("User checks TC and PP").exec(pp_and_tc)
  val check_user_exists_cycle = scenario("Check if the email exists").exec(check_email_exists)

  val campaign_contact_cycle = scenario("User checks campaign").exec(campaign_contact)
  val emi_partner_cycle = scenario("User emi partner").exec(emi_partner)
  val application_cycle = scenario("User gets application").exec(get_application)
  val drop_lead_cycle = scenario("User drops the lead").exec(drop_lead)
  val signup_cycle = scenario("User signs up").exec(signup)
  val signup_and_apply_cycle = scenario("User signsup and apply").exec(signup_and_apply)
  val create_application_cycle = scenario("User applies to program").exec(application)
  val authenticate_cycle = scenario("User gets authenticated").exec(authenticate)
  val check_lead_cycle = scenario("User checks lead").exec(lead)
  val login_v2_cycle = scenario("User logs in using V2").exec(login_v2)
  val login_v3_cycle = scenario("User logs in using V3").exec(login_v3)
  val get_all_batches_cycle = scenario("Get all the batches").exec(get_all_batches)
  val get_batch_by_pp_cycle = scenario("get batch by program package").exec(get_batch_package)
  val update_user_info_cycle = scenario("User update user info").exec(update_user_info)
  val get_user_info_cycle = scenario("User gets profile info").exec(user_info)
  val generate_verify_token_cycle = scenario("User gets generates and verifies the token").exec(one_time_token_generate, one_time_token_verify)

  setUp(
    pp_and_tc_cycle.inject(
      rampUsers(rampUpPP) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    campaign_contact_cycle.inject(
      rampUsers(rampUpUsersC) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    emi_partner_cycle.inject(
      rampUsers(rampUpEmi) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    application_cycle.inject(
      rampUsers(rampUpApplication) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    drop_lead_cycle.inject(
      rampUsers(rampUpdroplead) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    signup_cycle.inject(
      rampUsers(rampUpSignup) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    signup_and_apply_cycle.inject(
      rampUsers(rampUpignupApply) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    create_application_cycle.inject(
      rampUsers(rampUpCreateApplication) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    authenticate_cycle.inject(
      rampUsers(rampUpSignup) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    check_lead_cycle.inject(
      rampUsers(rampUpSignup) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    check_user_exists_cycle.inject(
      rampUsers(rampUpRL) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    login_v2_cycle.inject(
      rampUsers(rampUpUserSettings) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    login_v3_cycle.inject(
      rampUsers(rampUpUserSettings) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    get_all_batches_cycle.inject(
      rampUsers(rampUpApplication) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    get_batch_by_pp_cycle.inject(
      rampUsers(rampUpApplication) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    update_user_info_cycle.inject(
      rampUsers(rampUpApplication) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    get_user_info_cycle.inject(
      rampUsers(rampUpApplication) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    generate_verify_token_cycle.inject(
      rampUsers(rampUpApplication) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
