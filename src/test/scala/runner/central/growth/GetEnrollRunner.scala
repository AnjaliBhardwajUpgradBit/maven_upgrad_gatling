package runner.central.growth

import centralapi.learnapi.GetEnrollment._
import io.gatling.core.Predef._
import loadapi.Base._
class GetEnrollRunner extends Simulation {

  val user_life_cycle = scenario("User gets all the enrollment").exec(get_enrollment)
  setUp(
    user_life_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
