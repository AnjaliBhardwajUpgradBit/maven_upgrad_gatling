package runner.central.growth

import centralapi.authapi.LoginV2._
import io.gatling.core.Predef._
import loadapi.Base._

class LoginRunner extends Simulation {

  val login_cycle = scenario("User signs up").exec(login_v2)
  setUp(
    login_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
