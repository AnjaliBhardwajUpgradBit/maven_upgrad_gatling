package runner.central.growth

import centralapi.authapi.Authenticate._
import centralapi.authapi.SignupApply._
import centralapi.growthapi.CampaignContact._
import centralapi.growthapi.DropLead._
import centralapi.growthapi.EmiPartner._
import centralapi.growthapi.GetApplication._
import centralapi.growthapi.Lead._
import io.gatling.core.Predef._
import loadapi.Base._


class Sceanrio4Runner extends Simulation {

  val usersC = users.toInt
  val rampUpUsersC = rampUpUsers.toInt
  val users_10_percent = usersC/10
  val rampUpusers_10_percent = rampUpUsersC/10
  val users_5_percent = usersC/20
  val rampUpusers_5_percent = rampUpUsersC/20
  val campaign_contact_cycle = scenario("User checks campaign_contact").exec(campaign_contact)
  val emi_partner_cycle = scenario("User checks emi_partner").exec(emi_partner)
  val authenticate_cycle = scenario("User checks authenticate").exec(authenticate)
  val get_application_cycle = scenario("User checks get_application").exec(get_application)
  val lead_cycle = scenario("User checks lead").exec(lead)
  val drop_lead_cycle = scenario("User drops a lead").exec(drop_lead)
  val signup_and_apply_cycle = scenario("User signs up and apply").exec(signup_and_apply)
  setUp(
    campaign_contact_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol),
    emi_partner_cycle.inject(
      atOnceUsers(users_10_percent),
      rampUsers(rampUpusers_10_percent) during (rampUpTime seconds)
    ).protocols(httpProtocol),
    authenticate_cycle.inject(
      atOnceUsers(users_10_percent),
      rampUsers(rampUpusers_10_percent) during (rampUpTime seconds)
    ).protocols(httpProtocol),
    get_application_cycle.inject(
      atOnceUsers(users_10_percent),
      rampUsers(rampUpusers_10_percent) during (rampUpTime seconds)
    ).protocols(httpProtocol),
    lead_cycle.inject(
      atOnceUsers(users_10_percent),
      rampUsers(rampUpusers_10_percent) during (rampUpTime seconds)
    ).protocols(httpProtocol),
    drop_lead_cycle.inject(
      atOnceUsers(users_5_percent),
      rampUsers(rampUpusers_5_percent) during (rampUpTime seconds)
    ).protocols(httpProtocol),
    signup_and_apply_cycle.inject(
      atOnceUsers(users_5_percent),
      rampUsers(rampUpusers_5_percent) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
