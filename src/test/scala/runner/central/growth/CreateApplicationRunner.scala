package runner.central.growth

import centralapi.authapi.LoginV2._
import centralapi.growthapi.Application._
import io.gatling.core.Predef._
import loadapi.Base._


class CreateApplicationRunner extends Simulation {

  val application_cycle = scenario("User applies").exec(application)
  val login_cycle = scenario("User logs in").exec(application, login_v2)
  setUp(
    application_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
