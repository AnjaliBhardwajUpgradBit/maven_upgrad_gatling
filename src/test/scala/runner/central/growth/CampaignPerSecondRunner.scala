package runner.central.growth

import centralapi.growthapi.CampaignContact._
import io.gatling.core.Predef._
import loadapi.Base._

class CampaignPerSecondRunner extends Simulation {

  val constantUsersDouble = constantUsers.toDouble
  val campaign_cycle = scenario("User checks campaign number").exec(campaign_contact)
  setUp(
    campaign_cycle.inject(
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol))
}
