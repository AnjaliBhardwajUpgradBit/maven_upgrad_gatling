package runner.central.growth

import centralapi.growthapi.DropLead._
import io.gatling.core.Predef._
import loadapi.Base._

class DropLeadRunner extends Simulation {

  val drop_lead_cycle = scenario("User drops lead").exec(drop_lead)
  setUp(
    drop_lead_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
