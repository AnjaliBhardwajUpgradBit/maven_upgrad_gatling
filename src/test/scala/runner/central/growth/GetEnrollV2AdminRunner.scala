package runner.central.growth

import centralapi.learnapi.GetV2EnrollmentAdmin._
import io.gatling.core.Predef._
import loadapi.Base._

class GetEnrollV2AdminRunner extends Simulation {

  val user_life_cycle = scenario("User checks electives").exec(get_v2_enrollment_admin)
  setUp(
    user_life_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
