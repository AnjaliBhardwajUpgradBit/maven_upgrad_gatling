package runner.central.growth

import centralapi.growthapi.CampaignContact._
import centralapi.growthapi.EmiPartner._
import io.gatling.core.Predef._
import loadapi.Base._

class CampaignEmiRunner extends Simulation {

  val usersC = users.toInt
  val rampUpUsersC = rampUpUsers.toInt
  val users_campaign = usersC/10
  val rampUpUsers_campaign = rampUpUsersC/10
  val campaign_contact_cycle = scenario("User checks campaign").exec(campaign_contact)
  val emi_partner_cycle = scenario("User emi partner").exec(emi_partner)
  setUp(
    campaign_contact_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol),
    emi_partner_cycle.inject(
      atOnceUsers(users_campaign),
      rampUsers(rampUpUsers_campaign) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
