package runner.central.growth

import centralapi.authapi.Signup._
import centralapi.authapi.SignupApply._
import centralapi.growthapi.CampaignContact._
import centralapi.learnapi.GetAvailableCatalog._
import io.gatling.core.Predef._
import loadapi.Base._

class GrowthRunner extends Simulation {

  val signup_cycle = scenario("User signs up").exec(signup, signup_and_apply,campaign_contact, get_available_catalog)
  setUp(
    signup_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
