package runner.central.growth

import centralapi.authapi.Authenticate._
import centralapi.growthapi.CampaignContact._
import centralapi.growthapi.EmiPartner._
import centralapi.growthapi.GetApplication._
import centralapi.growthapi.Lead._
import io.gatling.core.Predef._
import loadapi.Base._

class Sceanrio3Runner extends Simulation {

  val usersC = users.toInt
  val rampUpUsersC = rampUpUsers.toInt
  val users_campaign = usersC/10
  val rampUpUsers_campaign = rampUpUsersC/10
  val campaign_contact_cycle = scenario("User checks campaign_contact").exec(campaign_contact)
  val emi_partner_cycle = scenario("User checks emi_partner").exec(emi_partner)
  val authenticate_cycle = scenario("User checks authenticate").exec(authenticate)
  val get_application_cycle = scenario("User checks get_application").exec(get_application)
  val lead_cycle = scenario("User checks lead").exec(lead)
  setUp(
    campaign_contact_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol),
    emi_partner_cycle.inject(
      atOnceUsers(users_campaign),
      rampUsers(rampUpUsers_campaign) during (rampUpTime seconds)
    ).protocols(httpProtocol),
    authenticate_cycle.inject(
      atOnceUsers(users_campaign),
      rampUsers(rampUpUsers_campaign) during (rampUpTime seconds)
    ).protocols(httpProtocol),
    get_application_cycle.inject(
      atOnceUsers(users_campaign),
      rampUsers(rampUpUsers_campaign) during (rampUpTime seconds)
    ).protocols(httpProtocol),
    lead_cycle.inject(
      atOnceUsers(users_campaign),
      rampUsers(rampUpUsers_campaign) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
