package runner.central.growth

import centralapi.growthapi.OneTimeToken._
import io.gatling.core.Predef._
import loadapi.Base._

class OneTimeTokenRunner extends Simulation {

  val one_time_token = scenario("User creates and verifies the token").exec(one_time_token_generate, one_time_token_verify)
  setUp(
    one_time_token.inject(
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol_staging_central))
}
