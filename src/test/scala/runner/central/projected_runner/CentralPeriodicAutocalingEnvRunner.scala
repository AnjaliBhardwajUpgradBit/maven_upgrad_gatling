package runner.central.projected_runner

import centralapi.authapi.Authenticate._
import centralapi.authapi.CheckEmailExists._
import centralapi.authapi.LoginV2._
import centralapi.authapi.LoginV3._
import centralapi.authapi.PpandTc._
import centralapi.authapi.Signup._
import centralapi.authapi.SignupApply._
import centralapi.growthapi.Application._
import centralapi.growthapi.CampaignContact._
import centralapi.growthapi.DropLead._
import centralapi.growthapi.EmiPartner._
import centralapi.growthapi.GetAllBatches._
import centralapi.growthapi.GetApplication._
import centralapi.growthapi.GetBatchByPP._
import centralapi.growthapi.Lead._
import centralapi.growthapi.OneTimeToken._
import centralapi.growthapi.UpdateUserInfo._
import centralapi.growthapi.UserInfo._
import centralapi.learnapi.GetAchievementStat._
import centralapi.learnapi.GetAvailableCatalog._
import centralapi.learnapi.GetCourseProgress._
import centralapi.learnapi.GetCurrentProgress._
import centralapi.learnapi.GetEnrollment._
import centralapi.learnapi.GetLmsConfig._
import centralapi.learnapi.GetModuleProgress._
import centralapi.learnapi.GetSegmentProgress._
import centralapi.learnapi.GetUserSettings._
import centralapi.learnapi.ModuleGroup._
import centralapi.learnapi.ModuleGroupById._
import centralapi.learnapi.RecentLogin._
import centralapi.learnapi.SkippedComponent._
import cloudfrontapi.DegreePage._
import cloudfrontapi.Home._
import io.gatling.core.Predef._
import loadapi.Base._

import scala.concurrent.duration.DurationInt

class CentralPeriodicAutocalingEnvRunner extends Simulation {

  val constantUsersDouble = constantUsers.toDouble


  val rampUpUserCurrentProgress = (constantUsersDouble*0.2032).toDouble

  val rampUpUsersC = (constantUsersDouble*0.2036).toDouble


  val rampUpPP = (constantUsersDouble*0.0209).toDouble
  val rampUpRL = (constantUsersDouble*0.1117).toDouble
  val rampUpSkipped = (constantUsersDouble*0.1118).toDouble

  val rampUpLMS = (constantUsersDouble*0.16).toDouble
  //Not in data

  val rampUpUserSettings = (constantUsersDouble*0.0074).toDouble
  val rampUpCourseProgress = (constantUsersDouble*0.1635).toDouble
  val rampUpModuleProgress = (constantUsersDouble*0.0766).toDouble

  val rampUpSegmentProgress = (constantUsersDouble*0.13).toDouble
  //Not in data

  val rampUpAvailableCatalog = (constantUsersDouble*0.0152).toDouble
  val rampUpModulegroup = (constantUsersDouble*0.0052).toDouble
  val rampUpModulegroupById = (constantUsersDouble*0.00087).toDouble
  val rampUpGetEnrollment = (constantUsersDouble*0.0178).toDouble
  val rampUpAchievement = (constantUsersDouble*0.00179).toDouble


  val rampUpEmi = (constantUsersDouble*0.0046).toDouble
  val rampUpApplication = (constantUsersDouble*0.0129).toDouble
  val rampUpdroplead = (constantUsersDouble*0.0204).toDouble
  val rampUpSignup = (constantUsersDouble*0.005).toDouble
  val rampUpignupApply = (constantUsersDouble*0.000048).toDouble
  val rampUpCreateApplication = (constantUsersDouble*0.00152).toDouble

  val pp_and_tc_cycle = scenario("User checks TC and PP").exec(pp_and_tc)
  val recent_login_cycle = scenario("User checks recent login").exec(recent_login)
  val skipped_component_cycle = scenario("User checks skipped components").exec(skipped_component)
  val get_lms_config_cycle = scenario("User checks his sound and video config").exec(get_lms_config)
  val get_user_settings_cycle = scenario("User checks permissions").exec(get_user_settings)
  val get_course_progress_cycle = scenario("User checks course progress").exec(get_course_progress)
  val get_current_progress_cycle = scenario("User checks segment page and current progress").exec(get_segment_page, get_current_progress)
  val get_module_progress_cycle = scenario("User checks module progress").exec(get_module_progress)
  val get_segment_progress_cycle = scenario("User checks segment progress").exec(get_segment_progress)
  val get_available_catalog_cycle = scenario("User checks available catalog").exec(get_available_catalog)
  val modulegroup_cycle = scenario("User checks all module groups").exec(modulegroup)
  val modulegroup_byid_cycle = scenario("User checks module group by id").exec(modulegroup_byid)
  val get_enrollment_cycle = scenario("User checks enrollments in courses").exec(get_enrollment)
  val get_achievement_stat_cycle = scenario("User checks the badges").exec(get_achievement_stat)
  val check_user_exists_cycle = scenario("Check if the email exists").exec(check_email_exists)

  val campaign_contact_cycle = scenario("User checks campaign").exec(campaign_contact)
  val emi_partner_cycle = scenario("User emi partner").exec(emi_partner)
  val application_cycle = scenario("User gets application").exec(get_application)
  val drop_lead_cycle = scenario("User drops the lead").exec(drop_lead)
  val signup_cycle = scenario("User signs up").exec(signup)
  val signup_and_apply_cycle = scenario("User signsup and apply").exec(signup_and_apply)
  val create_application_cycle = scenario("User applies to program").exec(application)
  val authenticate_cycle = scenario("User gets authenticated").exec(authenticate)
  val check_lead_cycle = scenario("User checks lead").exec(lead)
  val login_v2_cycle = scenario("User logs in using V2").exec(login_v2)
  val login_v3_cycle = scenario("User logs in using V3").exec(login_v3)
  val get_all_batches_cycle = scenario("Get all the batches").exec(get_all_batches)
  val get_batch_by_pp_cycle = scenario("get batch by program package").exec(get_batch_package)
  val update_user_info_cycle = scenario("User update user info").exec(update_user_info)
  val get_user_info_cycle = scenario("User gets profile info").exec(user_info)
  val generate_verify_token_cycle = scenario("User gets generates and verifies the token").exec(one_time_token_generate, one_time_token_verify)

  val cloudfront_home_cycle = scenario("User checks home on cloudfront").exec(home)
  val cloudfront_degree_cycle = scenario("User checks degree on cloudfront").exec(degree_page)

  setUp(
    pp_and_tc_cycle.inject(
      constantUsersPerSec(rampUpPP) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*3) during (1800 seconds),
      constantUsersPerSec(rampUpPP*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpPP) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    recent_login_cycle.inject(
      constantUsersPerSec(rampUpRL) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*3) during (1800 seconds),
      constantUsersPerSec(rampUpRL*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    skipped_component_cycle.inject(
      constantUsersPerSec(rampUpSkipped) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*3) during (1800 seconds),
      constantUsersPerSec(rampUpSkipped*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSkipped) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    get_lms_config_cycle.inject(
      constantUsersPerSec(rampUpLMS) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*3) during (1800 seconds),
      constantUsersPerSec(rampUpLMS*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpLMS) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    get_user_settings_cycle.inject(
      constantUsersPerSec(rampUpUserSettings) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*3) during (1800 seconds),
      constantUsersPerSec(rampUpUserSettings*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    get_course_progress_cycle.inject(
      constantUsersPerSec(rampUpCourseProgress) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*3) during (1800 seconds),
      constantUsersPerSec(rampUpCourseProgress*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCourseProgress) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    get_current_progress_cycle.inject(
      constantUsersPerSec(rampUpUserCurrentProgress) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*3) during (1800 seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    get_module_progress_cycle.inject(
      constantUsersPerSec(rampUpUserCurrentProgress) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*3) during (1800 seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserCurrentProgress) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    get_segment_progress_cycle.inject(
      constantUsersPerSec(rampUpSegmentProgress) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*3) during (1800 seconds),
      constantUsersPerSec(rampUpSegmentProgress*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSegmentProgress) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    get_available_catalog_cycle.inject(
      constantUsersPerSec(rampUpAvailableCatalog) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*3) during (1800 seconds),
      constantUsersPerSec(rampUpAvailableCatalog*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAvailableCatalog) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    modulegroup_cycle.inject(
      constantUsersPerSec(rampUpModulegroup) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*3) during (1800 seconds),
      constantUsersPerSec(rampUpModulegroup*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroup) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    modulegroup_byid_cycle.inject(
      constantUsersPerSec(rampUpModulegroupById) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*3) during (1800 seconds),
      constantUsersPerSec(rampUpModulegroupById*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpModulegroupById) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    get_enrollment_cycle.inject(
      constantUsersPerSec(rampUpGetEnrollment) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*3) during (1800 seconds),
      constantUsersPerSec(rampUpGetEnrollment*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpGetEnrollment) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    get_achievement_stat_cycle.inject(
      constantUsersPerSec(rampUpAchievement) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*3) during (1800 seconds),
      constantUsersPerSec(rampUpAchievement*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpAchievement) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),


    campaign_contact_cycle.inject(
      constantUsersPerSec(rampUpUsersC) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*3) during (1800 seconds),
      constantUsersPerSec(rampUpUsersC*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUsersC) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    emi_partner_cycle.inject(
      constantUsersPerSec(rampUpEmi) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*3) during (1800 seconds),
      constantUsersPerSec(rampUpEmi*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpEmi) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    application_cycle.inject(
      constantUsersPerSec(rampUpApplication) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*3) during (1800 seconds),
      constantUsersPerSec(rampUpApplication*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    drop_lead_cycle.inject(
      constantUsersPerSec(rampUpdroplead) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*3) during (1800 seconds),
      constantUsersPerSec(rampUpdroplead*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpdroplead) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    signup_cycle.inject(
      constantUsersPerSec(rampUpSignup) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*3) during (1800 seconds),
      constantUsersPerSec(rampUpSignup*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    signup_and_apply_cycle.inject(
      constantUsersPerSec(rampUpignupApply) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*3) during (1800 seconds),
      constantUsersPerSec(rampUpignupApply*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpignupApply) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    create_application_cycle.inject(
      constantUsersPerSec(rampUpCreateApplication) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*3) during (1800 seconds),
      constantUsersPerSec(rampUpCreateApplication*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpCreateApplication) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    authenticate_cycle.inject(
      constantUsersPerSec(rampUpSignup) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*3) during (1800 seconds),
      constantUsersPerSec(rampUpSignup*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    check_lead_cycle.inject(
      constantUsersPerSec(rampUpSignup) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*3) during (1800 seconds),
      constantUsersPerSec(rampUpSignup*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpSignup) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    check_user_exists_cycle.inject(
      constantUsersPerSec(rampUpRL) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*3) during (1800 seconds),
      constantUsersPerSec(rampUpRL*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    login_v2_cycle.inject(
      constantUsersPerSec(rampUpUserSettings) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*3) during (1800 seconds),
      constantUsersPerSec(rampUpUserSettings*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    login_v3_cycle.inject(
      constantUsersPerSec(rampUpUserSettings) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*3) during (1800 seconds),
      constantUsersPerSec(rampUpUserSettings*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpUserSettings) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    get_all_batches_cycle.inject(
      constantUsersPerSec(rampUpApplication) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*3) during (1800 seconds),
      constantUsersPerSec(rampUpApplication*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    get_batch_by_pp_cycle.inject(
      constantUsersPerSec(rampUpApplication) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*3) during (1800 seconds),
      constantUsersPerSec(rampUpApplication*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    update_user_info_cycle.inject(
      constantUsersPerSec(rampUpApplication) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*3) during (1800 seconds),
      constantUsersPerSec(rampUpApplication*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    get_user_info_cycle.inject(
      constantUsersPerSec(rampUpApplication) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*3) during (1800 seconds),
      constantUsersPerSec(rampUpApplication*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    generate_verify_token_cycle.inject(
      constantUsersPerSec(rampUpApplication) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*3) during (1800 seconds),
      constantUsersPerSec(rampUpApplication*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpApplication) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),

    cloudfront_home_cycle.inject(
      constantUsersPerSec(rampUpRL) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*3) during (1800 seconds),
      constantUsersPerSec(rampUpRL*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol_cloudfront_mercury),

    cloudfront_degree_cycle.inject(
      constantUsersPerSec(rampUpRL) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*3) during (1800 seconds),
      constantUsersPerSec(rampUpRL*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(rampUpRL) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol_cloudfront_mercury))
}
