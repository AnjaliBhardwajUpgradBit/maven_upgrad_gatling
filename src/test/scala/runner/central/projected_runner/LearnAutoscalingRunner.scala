package runner.central.projected_runner

import centralapi.learnapi.GetAchievementStat._
import centralapi.learnapi.GetCourseProgress._
import centralapi.learnapi.GetCurrentProgress._
import centralapi.learnapi.GetEnrollment._
import centralapi.learnapi.ModuleGroup._
import centralapi.learnapi.ModuleGroupById._
import centralapi.learnapi.RecentLogin._
import io.gatling.core.Predef._
import loadapi.Base._


class LearnAutoscalingRunner extends Simulation {

  val constantUsersDouble = constantUsers.toDouble
  constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds)

  val recent_login_cycle = scenario("User checks recent login").exec(recent_login)
  val get_course_progress_cycle = scenario("User checks course progress").exec(get_course_progress)
  val get_current_progress_cycle = scenario("User checks segment page and current progress").exec(get_segment_page, get_current_progress)
  val get_enrollment_cycle = scenario("User checks enrollments in courses").exec(get_enrollment)
  val get_achievement_stat_cycle = scenario("User checks the badges").exec(get_achievement_stat)
  val modulegroup_cycle = scenario("User checks all module groups").exec(modulegroup)
  val modulegroup_byid_cycle = scenario("User checks module group by id").exec(modulegroup_byid)

  setUp(
    modulegroup_byid_cycle.inject(
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*3) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    recent_login_cycle.inject(
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*3) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    get_course_progress_cycle.inject(
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*3) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    get_current_progress_cycle.inject(
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*3) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    get_enrollment_cycle.inject(
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*3) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    get_achievement_stat_cycle.inject(
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*3) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol_learnapi_mercury),
    modulegroup_cycle.inject(
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*3) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol_learnapi_mercury))
}
