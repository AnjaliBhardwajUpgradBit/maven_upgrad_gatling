package runner.central.projected_runner

import centralapi.authapi.Authenticate._
import centralapi.authapi.CheckEmailExists._
import centralapi.authapi.LoginV2._
import centralapi.authapi.LoginV3._
import centralapi.authapi.PpandTc._
import centralapi.authapi.Signup._
import centralapi.authapi.SignupApply._
import centralapi.growthapi.Application._
import centralapi.growthapi.CampaignContact._
import centralapi.growthapi.DropLead._
import centralapi.growthapi.EmiPartner._
import centralapi.growthapi.GetAllBatches._
import centralapi.growthapi.GetApplication._
import centralapi.growthapi.GetBatchByPP._
import centralapi.growthapi.Lead._
import centralapi.growthapi.OneTimeToken._
import centralapi.growthapi.UpdateUserInfo._
import centralapi.growthapi.UserInfo._
import centralapi.learnapi.GetAchievementStat._
import centralapi.learnapi.GetAvailableCatalog._
import centralapi.learnapi.GetCourseProgress._
import centralapi.learnapi.GetCurrentProgress._
import centralapi.learnapi.GetEnrollment._
import centralapi.learnapi.GetLmsConfig._
import centralapi.learnapi.GetModuleProgress._
import centralapi.learnapi.GetSegmentProgress._
import centralapi.learnapi.GetUserSettings._
import centralapi.learnapi.ModuleGroup._
import centralapi.learnapi.ModuleGroupById._
import centralapi.learnapi.RecentLogin._
import centralapi.learnapi.SkippedComponent._
import cloudfrontapi.DegreePage._
import cloudfrontapi.Home._
import io.gatling.core.Predef._
import loadapi.Base._

class CentralApiLearnApiProjectedEnvRunner extends Simulation {

  //Runtime values
  val usersC = users.toInt
  val rampUpUserCurrentProgressS = rampUpUsers.toInt
  val rampUpUserCurrentProgress = (rampUpUserCurrentProgressS*1.20*0.8).toInt
  // Multiplier factor is expected DAU by max dau (15310/12758)
  val rampUpUsersC = (rampUpUserCurrentProgress*0.322*3.73).toInt
  // Multiplier factor is expected DAU by max dau (230000/61651)

  //Central Learn ratio
  val rampUpRL = (rampUpUserCurrentProgress*0.00565).toInt
  //1/100 for recent login
  val rampUpSkipped = (rampUpUserCurrentProgress*0.566).toInt
  val rampUpLMS = (rampUpUserCurrentProgress*0.41).toInt
  val rampUpUserSettings = (rampUpUserCurrentProgress*0.036).toInt
  val rampUpCourseProgress = (rampUpUserCurrentProgress*0.81).toInt
  val rampUpModuleProgress = (rampUpUserCurrentProgress*0.41).toInt
  val rampUpSegmentProgress = (rampUpUserCurrentProgress*0.3).toInt
  val rampUpAvailableCatalog = (rampUpUserCurrentProgress*0.084).toInt
  val rampUpModulegroup = (rampUpUserCurrentProgress*0.009).toInt
  val rampUpModulegroupById = (rampUpUserCurrentProgress*0.093).toInt
  val rampUpGetEnrollment = (rampUpUserCurrentProgress*0.097).toInt
  val rampUpAchievement = (rampUpUserCurrentProgress*0.011).toInt

  //Growth Learn ratio
  val rampUpUserCurrentProgress_learn = (rampUpUserCurrentProgressS*1.20*1.24).toInt
  val rampUpRL_learn = (rampUpUserCurrentProgress_learn*0.00565).toInt
  //1/100 for recent login
  val rampUpSkipped_learn = (rampUpUserCurrentProgress_learn*0.566).toInt
  val rampUpLMS_learn = (rampUpUserCurrentProgress_learn*0.41).toInt
  val rampUpUserSettings_learn = (rampUpUserCurrentProgress_learn*0.036).toInt
  val rampUpCourseProgress_learn = (rampUpUserCurrentProgress_learn*0.81).toInt
  val rampUpModuleProgress_learn = (rampUpUserCurrentProgress_learn*0.41).toInt
  val rampUpSegmentProgress_learn = (rampUpUserCurrentProgress_learn*0.3).toInt
  val rampUpAvailableCatalog_learn = (rampUpUserCurrentProgress_learn*0.084).toInt
  val rampUpModulegroup_learn = (rampUpUserCurrentProgress_learn*0.009).toInt
  val rampUpModulegroupById_learn = (rampUpUserCurrentProgress_learn*0.093).toInt
  val rampUpGetEnrollment_learn = (rampUpUserCurrentProgress_learn*0.097).toInt
  val rampUpAchievement_learn = (rampUpUserCurrentProgress_learn*0.011).toInt

  //Growth ratio
  val rampUpEmi = (rampUpUsersC*0.02).toInt
  val rampUpApplication = (rampUpUsersC*0.064).toInt
  val rampUpdroplead = (rampUpUsersC*0.048).toInt
  val rampUpSignup = (rampUpUsersC*0.025).toInt
  val rampUpignupApply = (rampUpUsersC*0.00022).toInt
  val rampUpCreateApplication = (rampUpUsersC*0.0073).toInt
  val rampUpPP = (rampUpUserCurrentProgress*0.099).toInt

  //Central Learn cycle
  val pp_and_tc_cycle = scenario("User checks TC and PP").exec(pp_and_tc)
  val recent_login_cycle = scenario("User checks recent login").exec(recent_login)
  val skipped_component_cycle = scenario("User checks skipped components").exec(skipped_component)
  val get_lms_config_cycle = scenario("User checks his sound and video config").exec(get_lms_config)
  val get_user_settings_cycle = scenario("User checks permissions").exec(get_user_settings)
  val get_course_progress_cycle = scenario("User checks course progress").exec(get_course_progress)
  val get_current_progress_cycle = scenario("User checks segment page and current progress").exec(get_segment_page, get_current_progress)
  val get_module_progress_cycle = scenario("User checks module progress").exec(get_module_progress)
  val get_segment_progress_cycle = scenario("User checks segment progress").exec(get_segment_progress)
  val get_available_catalog_cycle = scenario("User checks available catalog").exec(get_available_catalog)
  val modulegroup_cycle = scenario("User checks all module groups").exec(modulegroup)
  val modulegroup_byid_cycle = scenario("User checks module group by id").exec(modulegroup_byid)
  val get_enrollment_cycle = scenario("User checks enrollments in courses").exec(get_enrollment)
  val get_achievement_stat_cycle = scenario("User checks the badges").exec(get_achievement_stat)

  //Learn API cycle
  val recent_login_learn_cycle = scenario("LearnAPI: User checks recent login").exec(recent_login)
  val skipped_component_learn_cycle = scenario("LearnAPI: User checks skipped components").exec(skipped_component)
  val get_lms_config_learn_cycle = scenario("LearnAPI: User checks his sound and video config").exec(get_lms_config)
  val get_user_settings_learn_cycle = scenario("LearnAPI: User checks permissions").exec(get_user_settings)
  val get_course_progress_learn_cycle = scenario("LearnAPI: User checks course progress").exec(get_course_progress)
  val get_current_progress_learn_cycle = scenario("LearnAPI: User checks current progress").exec(get_current_progress)
  val get_module_progress_learn_cycle = scenario("LearnAPI: User checks module progress").exec(get_module_progress)
  val get_segment_progress_learn_cycle = scenario("LearnAPI: User checks segment progress").exec(get_segment_progress)
  val get_available_catalog_learn_cycle = scenario("LearnAPI: User checks available catalog").exec(get_available_catalog)
  val modulegroup_learn_cycle = scenario("LearnAPI: User checks all module groups").exec(modulegroup)
  val modulegroup_byid_learn_cycle = scenario("LearnAPI: User checks module group by id").exec(modulegroup_byid)
  val get_enrollment_learn_cycle = scenario("LearnAPI: User checks enrollments in courses").exec(get_enrollment)
  val get_achievement_stat_learn_cycle = scenario("LearnAPI: User checks the badges").exec(get_achievement_stat)

  //Cenrtal growth cycle
  val campaign_contact_cycle = scenario("User checks campaign").exec(campaign_contact)
  val emi_partner_cycle = scenario("User emi partner").exec(emi_partner)
  val application_cycle = scenario("User gets application").exec(get_application)
  val drop_lead_cycle = scenario("User drops the lead").exec(drop_lead)
  val signup_cycle = scenario("User signs up").exec(signup)
  val signup_and_apply_cycle = scenario("User signsup and apply").exec(signup_and_apply)
  val create_application_cycle = scenario("User applies to program").exec(application)
  val authenticate_cycle = scenario("User gets authenticated").exec(authenticate)
  val check_lead_cycle = scenario("User checks lead").exec(lead)
  val login_v2_cycle = scenario("User logs in using V2").exec(login_v2)
  val login_v3_cycle = scenario("User logs in using V3").exec(login_v3)
  val get_all_batches_cycle = scenario("Get all the batches").exec(get_all_batches)
  val get_batch_by_pp_cycle = scenario("get batch by program package").exec(get_batch_package)
  val update_user_info_cycle = scenario("User update user info").exec(update_user_info)
  val get_user_info_cycle = scenario("User gets profile info").exec(user_info)
  val check_user_exists_cycle = scenario("Check if the email exists").exec(check_email_exists)
  val generate_verify_token_cycle = scenario("User gets generates and verifies the token").exec(one_time_token_generate, one_time_token_verify)

  //Cloudfront cycle
  val cloudfront_home_cycle = scenario("Cloudfront: User checks home on cloudfront").exec(home)
  val cloudfront_degree_cycle = scenario("Cloudfront: User checks degree on cloudfront").exec(degree_page)

  setUp(
    //Central Learn APIs
    pp_and_tc_cycle.inject(
      rampUsers(rampUpPP) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    recent_login_cycle.inject(
      rampUsers(rampUpRL) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    skipped_component_cycle.inject(
      rampUsers(rampUpSkipped) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    get_lms_config_cycle.inject(
      rampUsers(rampUpLMS) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    get_user_settings_cycle.inject(
      rampUsers(rampUpUserSettings) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    get_course_progress_cycle.inject(
      rampUsers(rampUpCourseProgress) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    get_current_progress_cycle.inject(
      rampUsers(rampUpUserCurrentProgress) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    get_module_progress_cycle.inject(
      rampUsers(rampUpModuleProgress) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    get_segment_progress_cycle.inject(
      rampUsers(rampUpSegmentProgress) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    get_available_catalog_cycle.inject(
      rampUsers(rampUpAvailableCatalog) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    modulegroup_cycle.inject(
      rampUsers(rampUpModulegroup) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    modulegroup_byid_cycle.inject(
      rampUsers(rampUpModulegroupById) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    get_enrollment_cycle.inject(
      rampUsers(rampUpGetEnrollment) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    get_achievement_stat_cycle.inject(
      rampUsers(rampUpAchievement) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    //Central Marketing APIs
    campaign_contact_cycle.inject(
      rampUsers(rampUpUsersC) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    emi_partner_cycle.inject(
      rampUsers(rampUpEmi) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    application_cycle.inject(
      rampUsers(rampUpApplication) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    drop_lead_cycle.inject(
      rampUsers(rampUpdroplead) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    signup_cycle.inject(
      rampUsers(rampUpSignup) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    signup_and_apply_cycle.inject(
      rampUsers(rampUpignupApply) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    create_application_cycle.inject(
      rampUsers(rampUpCreateApplication) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    authenticate_cycle.inject(
      rampUsers(rampUpSignup) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    check_lead_cycle.inject(
      rampUsers(rampUpSignup) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    check_user_exists_cycle.inject(
      rampUsers(rampUpRL) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    login_v2_cycle.inject(
      rampUsers(rampUpUserSettings) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    login_v3_cycle.inject(
      rampUsers(rampUpUserSettings) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    get_all_batches_cycle.inject(
      rampUsers(rampUpApplication) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    get_batch_by_pp_cycle.inject(
      rampUsers(rampUpApplication) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    update_user_info_cycle.inject(
      rampUsers(rampUpApplication) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    get_user_info_cycle.inject(
      rampUsers(rampUpApplication) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    generate_verify_token_cycle.inject(
      rampUsers(rampUpApplication) during (rampUpTime seconds)
    ).protocols(httpProtocol),


    //Cloudfront APIs
    cloudfront_home_cycle.inject(
      rampUsers(rampUpRL) during (rampUpTime seconds)
    ).protocols(httpProtocol_cloudfront_mercury),

    cloudfront_degree_cycle.inject(
      rampUsers(rampUpRL) during (rampUpTime seconds)
    ).protocols(httpProtocol_cloudfront_mercury),

    //Learn APIs
    recent_login_learn_cycle.inject(
      rampUsers(rampUpRL_learn) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),

    skipped_component_learn_cycle.inject(
      rampUsers(rampUpSkipped_learn) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),

    get_lms_config_learn_cycle.inject(
      rampUsers(rampUpLMS_learn) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),

    get_user_settings_learn_cycle.inject(
      rampUsers(rampUpUserSettings_learn) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),

    get_course_progress_learn_cycle.inject(
      rampUsers(rampUpCourseProgress_learn) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),

    get_current_progress_learn_cycle.inject(
      rampUsers(rampUpUserCurrentProgress_learn) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),

    get_module_progress_learn_cycle.inject(
      rampUsers(rampUpModuleProgress_learn) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),

    get_segment_progress_learn_cycle.inject(
      rampUsers(rampUpSegmentProgress_learn) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),

    get_available_catalog_learn_cycle.inject(
      rampUsers(rampUpAvailableCatalog_learn) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),

    modulegroup_learn_cycle.inject(
      rampUsers(rampUpModulegroup_learn) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),

    modulegroup_byid_learn_cycle.inject(
      rampUsers(rampUpModulegroupById_learn) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),

    get_enrollment_learn_cycle.inject(
      rampUsers(rampUpGetEnrollment_learn) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury),

    get_achievement_stat_learn_cycle.inject(
      rampUsers(rampUpAchievement_learn) during (rampUpTime seconds)
    ).protocols(httpProtocol_learnapi_mercury)
    )
}
