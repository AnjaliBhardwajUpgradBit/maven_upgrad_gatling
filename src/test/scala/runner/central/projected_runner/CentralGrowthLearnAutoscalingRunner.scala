package runner.central.projected_runner

import centralapi.authapi.PpandTc._
import centralapi.authapi.Signup._
import centralapi.growthapi.CampaignContact._
import centralapi.growthapi.EmiPartner._
import centralapi.learnapi.GetCourseProgress._
import centralapi.learnapi.GetCurrentProgress._
import centralapi.learnapi.RecentLogin._
import io.gatling.core.Predef._
import loadapi.Base._

class CentralGrowthLearnAutoscalingRunner extends Simulation {

  val constantUsersDouble = constantUsers.toDouble
  constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds)

  val pp_and_tc_cycle = scenario("User checks TC and PP").exec(pp_and_tc)
  val recent_login_cycle = scenario("User checks recent login").exec(recent_login)
  val get_course_progress_cycle = scenario("User checks course progress").exec(get_course_progress)
  val get_current_progress_cycle = scenario("User checks segment page and current progress").exec(get_segment_page, get_current_progress)

  val campaign_contact_cycle = scenario("User checks campaign").exec(campaign_contact)
  val emi_partner_cycle = scenario("User emi partner").exec(emi_partner)
  val signup_cycle = scenario("User signs up").exec(signup)

  setUp(
    pp_and_tc_cycle.inject(
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*3) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),
    recent_login_cycle.inject(
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*3) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),
    get_course_progress_cycle.inject(
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*3) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),
    get_current_progress_cycle.inject(
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*3) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),
    campaign_contact_cycle.inject(
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*3) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),
    emi_partner_cycle.inject(
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*3) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol),
    signup_cycle.inject(
      constantUsersPerSec(constantUsersDouble) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*1.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.16) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.33) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.5) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.67) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*2.83) during (timeForConstantUsers seconds),
      constantUsersPerSec(constantUsersDouble*3) during (timeForConstantUsers seconds)
    ).protocols(httpProtocol))
}
