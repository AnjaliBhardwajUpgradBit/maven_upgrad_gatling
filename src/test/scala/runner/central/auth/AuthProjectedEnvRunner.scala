package runner.central.auth

import centralapi.authapi.Authenticate._
import centralapi.authapi.LoginV2._
import centralapi.authapi.LoginV3._
import centralapi.authapi.PpandTc._
import centralapi.authapi.Signup._
import centralapi.authapi.SignupApply._
import io.gatling.core.Predef._
import loadapi.Base._

class AuthProjectedEnvRunner extends Simulation {

  val usersC = users.toInt
  val rampUpUserCurrentProgressS = rampUpUsers.toInt
  val rampUpUserCurrentProgress = (rampUpUserCurrentProgressS*1.20).toInt
  // Multiplier factor is expected DAU by max dau (15310/12758)
  val rampUpUsersC = (rampUpUserCurrentProgress*0.322*3.73).toInt
  // Multiplier factor is expected DAU by max dau (230000/61651)

  val rampUpPP = (rampUpUserCurrentProgress*0.099).toInt
  //reduced the frequency by 1/100 of RL
  val rampUpRL = (rampUpUserCurrentProgress*0.565).toInt

  val rampUpSignup = (rampUpUsersC*0.025).toInt
  val rampUpignupApply = (rampUpUsersC*0.00022).toInt
  val rampUpUserSettings = (rampUpUserCurrentProgress*0.036).toInt

  val pp_and_tc_cycle = scenario("User checks TC and PP").exec(pp_and_tc)
  val signup_cycle = scenario("User signs up").exec(signup)
  val signup_and_apply_cycle = scenario("User signsup and apply").exec(signup_and_apply)
  val authenticate_cycle = scenario("User gets authenticated").exec(authenticate)
  val login_v2_cycle = scenario("User logs in using V2").exec(login_v2)
  val login_v3_cycle = scenario("User logs in using V3").exec(login_v3)

  setUp(
    pp_and_tc_cycle.inject(
      rampUsers(rampUpPP) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    signup_cycle.inject(
      rampUsers(rampUpSignup) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    signup_and_apply_cycle.inject(
      rampUsers(rampUpignupApply) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    authenticate_cycle.inject(
      rampUsers(rampUpSignup) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    login_v2_cycle.inject(
      rampUsers(rampUpUserSettings) during (rampUpTime seconds)
    ).protocols(httpProtocol),

    login_v3_cycle.inject(
      rampUsers(rampUpUserSettings) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
