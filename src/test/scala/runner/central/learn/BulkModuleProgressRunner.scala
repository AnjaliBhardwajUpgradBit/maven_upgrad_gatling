package runner.central.learn

import centralapi.learnapi.GetModuleProgress._
import io.gatling.core.Predef._
import loadapi.Base._


class BulkModuleProgressRunner extends Simulation {

  val user_life_cycle = scenario("User access important APIS of CE").exec(get_module_progress)
  setUp(
    user_life_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
