package runner.central.learn

import centralapi.authapi.Enroll._
import centralapi.authapi.LoginV2._
import centralapi.authapi.Signup._
import centralapi.growthapi.Application._
import centralapi.growthapi.ProgramPage._
import centralapi.learnapi.Course._
import io.gatling.core.Predef._
import loadapi.Base._

import scala.concurrent.duration.DurationInt

class UserLifeCycleRunner extends Simulation {
  val user_life_cycle = scenario("User access a single page").exec(signup, program_page, application, enroll, login_v2, course)
  setUp(
    user_life_cycle.inject(
      atOnceUsers(3000),
      rampUsers(400) during (10 seconds)
    ).protocols(httpProtocol))
}
