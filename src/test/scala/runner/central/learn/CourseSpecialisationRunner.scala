package runner.central.learn

import centralapi.learnapi.ElectiveConfiguration._
import centralapi.learnapi.GetEnrollment._
import centralapi.learnapi.ModuleGroup._
import centralapi.learnapi.ModuleGroupById._
import io.gatling.core.Predef._
import loadapi.Base._


class CourseSpecialisationRunner extends Simulation {

  val user_life_cycle = scenario("User access important APIS of CE").exec(elective_configuration, modulegroup, modulegroup_byid, get_enrollment)
  setUp(
    user_life_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
