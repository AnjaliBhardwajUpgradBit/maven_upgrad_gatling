package runner.central.learn

import centralapi.learnapi.ElectiveConfiguration._
import io.gatling.core.Predef._
import loadapi.Base._


class ProductionUserBehaviourRunner extends Simulation {

  val user_life_cycle = scenario("User access important APIS of CE").exec(elective_configuration)
  setUp(
    user_life_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
