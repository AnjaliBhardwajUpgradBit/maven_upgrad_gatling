package runner.central.learn

import centralapi.learnapi.GetSegmentProgress._
import io.gatling.core.Predef._
import loadapi.Base._


class BulkSegmentProgressRunner extends Simulation {

  val user_life_cycle = scenario("User access important APIS of CE").exec(get_segment_progress)
  setUp(
    user_life_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
