package runner.central.learn

import centralapi.learnapi.ModuleGroup._
import io.gatling.core.Predef._
import loadapi.Base._

class ModuleGroupRunner extends Simulation {

  val user_life_cycle = scenario("User checks modulegroups").exec(modulegroup)
  setUp(
    user_life_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
