package runner.central.learn

import centralapi.learnapi.PopulateModuleProgress._
import io.gatling.core.Predef._
import loadapi.Base._

class PopulateModuleProgressRunner extends Simulation {

  val user_life_cycle = scenario("Admin populates module group progress").exec(populate_module_progress)
  setUp(
    user_life_cycle.inject(
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
