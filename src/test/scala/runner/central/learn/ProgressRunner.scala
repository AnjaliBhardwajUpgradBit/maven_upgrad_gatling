package runner.central.learn

import centralapi.learnapi.GetAvailableCatalog._
import centralapi.learnapi.GetCourseProgress._
import centralapi.learnapi.GetCurrentProgress._
import centralapi.learnapi.GetModuleProgress._
import centralapi.learnapi.GetSegmentProgress._
import io.gatling.core.Predef._
import loadapi.Base._


class ProgressRunner extends Simulation {

  val user_life_cycle = scenario("User access important APIS of CE").exec(get_available_catalog, get_current_progress, get_module_progress, get_course_progress, get_segment_progress)
  setUp(
    user_life_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
