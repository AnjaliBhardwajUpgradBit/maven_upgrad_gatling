package runner.central.learn

import centralapi.learnapi.ModuleGroupById._
import io.gatling.core.Predef._
import loadapi.Base._

class ModuleGroupByIdRunner extends Simulation {

  val user_life_cycle = scenario("User checks module groups by id").exec(modulegroup_byid)
  setUp(
    user_life_cycle.inject(
      atOnceUsers(users),
      rampUsers(rampUpUsers) during (rampUpTime seconds)
    ).protocols(httpProtocol))
}
