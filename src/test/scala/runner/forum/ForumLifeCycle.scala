package runner.forum

import forumapi.ForumStats._
import forumapi.GetAllQuestions._
import forumapi.GetContentQuestion._
import forumapi.GetLeaderboard._
import forumapi.GetSeeded._
import forumapi.GetV1Answers._
import forumapi.GetV1Contexts._
import forumapi.GetV1Users._
import forumapi.GetV2Questions._
import forumapi.PostV1Answers._
import io.gatling.core.Predef._
import loadapi.Base._

class ForumLifeCycleRunner extends Simulation {
  val usersC = users.toInt
  val rampAllQuestions = rampUpUsers.toInt
  //val rampQuizsessionbulkV2 = (rampQuizsessionbulk*1.20).toInt

  val rampForumStats = (rampAllQuestions*0.196).toInt
  val rampV2Questions = (rampAllQuestions*0.096).toInt
  val rampV1Answer = (rampAllQuestions*0.094).toInt
  val rampV1Users = (rampAllQuestions*0.061).toInt
  val rampLeaderboard = (rampAllQuestions*0.049).toInt
  val rampContentQuestions = (rampAllQuestions*0.038).toInt
  val rampV1Context = (rampAllQuestions*0.022).toInt
  val rampV1Answers = (rampAllQuestions*0.016).toInt
  val rampSeeded = (rampAllQuestions*0.008).toInt

  val get_all_questions_cycle = scenario("User gets all v3 questions").exec(get_all_questions)
  val get_forum_stats_cycle = scenario("User gets forum stats").exec(get_forum_stats)
  val get_v2_questions_cycle = scenario("User gets v2 questions").exec(get_v2_questions)
  val get_v1_answers_cycle = scenario("User v1 answers").exec(get_v1_answers)
  val get_v1_users_cycle = scenario("User v1 users").exec(get_v1_users)
  val get_leaderboard_cycle = scenario("User gets leaderboard").exec(get_leaderboard)
  val get_content_questions_cycle = scenario("User gets content questions").exec(get_content_questions)
  val get_v1_contexts_cycle = scenario("User gets v1 contexts").exec(get_v1_contexts)
  val post_v1_answers_cycle = scenario("User posts v1 answers").exec(post_v1_answers)
  val get_seeded_cycle = scenario("User gets seeded data").exec(get_seeded)

  setUp(
    get_all_questions_cycle.inject(
      rampUsers(rampAllQuestions) during (rampUpTime seconds)
    ).protocols(httpProtocol_forum_mercury),
    get_forum_stats_cycle.inject(
      rampUsers(rampForumStats) during (rampUpTime seconds)
    ).protocols(httpProtocol_forum_mercury),
    get_v2_questions_cycle.inject(
      rampUsers(rampV2Questions) during (rampUpTime seconds)
    ).protocols(httpProtocol_forum_mercury),
    get_v1_answers_cycle.inject(
      rampUsers(rampV1Answer) during (rampUpTime seconds)
    ).protocols(httpProtocol_forum_mercury),
    get_v1_users_cycle.inject(
      rampUsers(rampV1Users) during (rampUpTime seconds)
    ).protocols(httpProtocol_forum_mercury),
    get_leaderboard_cycle.inject(
      rampUsers(rampLeaderboard) during (rampUpTime seconds)
    ).protocols(httpProtocol_forum_mercury),
    get_content_questions_cycle.inject(
      rampUsers(rampContentQuestions) during (rampUpTime seconds)
    ).protocols(httpProtocol_forum_mercury),
    get_v1_contexts_cycle.inject(
      rampUsers(rampV1Context) during (rampUpTime seconds)
    ).protocols(httpProtocol_forum_mercury),
    post_v1_answers_cycle.inject(
      rampUsers(rampV1Answers) during (rampUpTime seconds)
    ).protocols(httpProtocol_forum_mercury),
    get_seeded_cycle.inject(
      rampUsers(rampSeeded) during (rampUpTime seconds)
    ).protocols(httpProtocol_forum_mercury)
  )
}
