package runner.profanity

import io.gatling.core.Predef._
import loadapi.Base._
import profanityapi.Profanitycheck._

class Profanitycheck extends Simulation {

  val usersC = users.toInt
  val rampUserProfileV2 = rampUpUsers.toInt

val profanity_cycle = scenario("User runs profanity").exec(profanity_check)
  setUp(
    profanity_cycle.inject(
      rampUsers(rampUserProfileV2) during (rampUpTime seconds)
    ).protocols(httpProtocol_profanity)
  )
}
