package profanityapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object Profanitycheck {
  val profanity_check = exec(http("User checks for profanity")
    .post("/profanity")
    .headers(Map("Content-Type" -> "application/json"))
    .body(StringBody("""{"text": "One simplified way you could think about why This library is far from perfect. For example, Never treat any prediction from this library as unquestionable truth. Fuck off. because it does and will make mistakes words out of all possible words and using those to make future predictions. This is better than just re"}""")).asJson
    .check(status.is(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "400" || session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of profanity api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
