package gradingapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object QuizModuleListing {
  val quiz_module_listing = feed(paidActiveUser).exec(http("User gets quiz module listing")
    .get("/admin-panel/quiz-module/${course_id}/module/${module_id}")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of quiz module listing api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
