package gradingapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object PenaltyOverride {
  val pently_override = feed(paidActiveUser).exec(http("User gets penalty override")
    .get("/penalty-overrides/learner/course/${course_id}/user/${user_id}")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of penalty override api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
