package gradingapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object MarkAsSeen {
  val mark_as_seen = feed(paidActiveUser).exec(http("User mark as seen")
    .get("/score/mark-as-seen")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of mark as seen api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
