package gradingapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GradeConfig {
  val grade_config = feed(paidActiveUser).exec(http("User gets grade configuration")
    .get("/configuration/course/${course_id}")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of grade config api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
