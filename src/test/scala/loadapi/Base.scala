package loadapi

import io.gatling.http.Predef.http
import io.gatling.core.Predef._

import scala.util.Random

object Base {

  val users = java.lang.Integer.getInteger("users", 1)
  val rampUpUsers = java.lang.Integer.getInteger("rampUpUsers", 1)
  val rampUpTime = java.lang.Integer.getInteger("rampUpTime", 1)
  val constantUsers = java.lang.Integer.getInteger("constantUsers", 1)
  val timeForConstantUsers = java.lang.Integer.getInteger("timeForConstantUsers", 1)
  val envC = System.getProperty("testServerUrl", "https://mercury-central-service.upgrad.com")

  val acceptH = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
  val doNotTrackH = "1"
  val acceptLanguageH = "en-US,en;q=0.5"
  val acceptEncodingH = "gzip, deflate"
  val userAgentH = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0"

  val httpProtocol = http
    .baseUrl(envC)
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val httpProtocol_staging_central = http
    .baseUrl("https://stagingapi.upgrad.com")
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val httpProtocol_saturn = http
    .baseUrl("https://saturn-central-service.upgrad.com")
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val httpProtocol_uranus = http
    .baseUrl("https://uranus-central-service.upgrad.com")
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val httpProtocol_mercury = http
    .baseUrl("https://mercury-central-service.upgrad.com")
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val httpProtocol_learnapi_mercury = http
    .baseUrl("https://mercury-learn-api.upgrad.com")
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val httpProtocol_cloudfront_mercury = http
    .baseUrl("https://mercury-marketing-web.upgrad.com")
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val httpProtocol_prod = http
    .baseUrl("https://prodapi.upgrad.com")
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val httpProtocol_assessment = http
    .baseUrl("https://stagingassessmentsapi.upgrad.com")
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val httpProtocol_assessment_mercury = http
    .baseUrl("https://mercury-assessments-service.upgrad.com")
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val httpProtocol_forum = http
    .baseUrl("https://stagingforumapi.upgrad.com")
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val httpProtocol_forum_mercury = http
    .baseUrl("https://mercury-forum-service.upgrad.com")
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val httpProtocol_grading_mercury = http
    .baseUrl("https://mercury-grading-service.upgrad.com/apis")
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val httpProtocol_career = http
    .baseUrl("https://stagingcareersapi.upgrad.com")
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val httpProtocol_career_mercury = http
    .baseUrl("https://mercury-careers-service.upgrad.com")
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val httpProtocol_profanity = http
    .baseUrl("https://mercury-profanity-service.upgrad.com")
    .acceptHeader(acceptH)
    .doNotTrackHeader(doNotTrackH)
    .acceptLanguageHeader(acceptLanguageH)
    .acceptEncodingHeader(acceptEncodingH)
    .userAgentHeader(userAgentH)

  val contentType = "application/x-www-form-urlencoded"
  val adminAuth = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJ1c2VybmFtZVwiOlwiY2hlY2ttYXRlX3NlcnZpY2UraW50ZXJuYWxhcHBAdXBncmFkLmNvbVwiLFwic2Vzc2lvbklEXCI6XCIxNDQ3OTQ1NzgxNjkxNzU3MjMyMzk4OTU0MTk5MDE5NDM3NjQyOTZcIn0ifQ.96FNxRIq4iwuLA4S2oDbfifiztEfqL2F5AS6hO7iULNhvztIvV8D5fFtpv1oFNdINYqA67LoAKV5YhFdYp5RZQ"
  val adminSession = "1QBBhe3jTEmZRqAdsadasLWgdwo3e"
  val assessmentAuth = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJ1c2VybmFtZVwiOlwiY2hlY2ttYXRlX3NlcnZpY2UraW50ZXJuYWxhcHBAdXBncmFkLmNvbVwiLFwic2Vzc2lvbklEXCI6XCIxNDQ3OTQ1NzgxNjkxNzU3MjMyMzk4OTU0MTk5MDE5NDM3NjQyOTZcIn0ifQ.96FNxRIq4iwuLA4S2oDbfifiztEfqL2F5AS6hO7iULNhvztIvV8D5fFtpv1oFNdINYqA67LoAKV5YhFdYp5RZQ"
  val assessmentSession = "1G3yBDWdx4RPkHDtytpdmpAvf6a5B6vFVB"
  val assessmentCourse = "4674"
  val assessmentStudentAuth = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJ1c2VybmFtZVwiOlwicnVwZXJ0QHppZW1hbm5tYW5uLm9yZ1wiLFwic2Vzc2lvbklEXCI6XCI3YzNhNTkxNi0zOTIxLTRjYzAtOTliMy1jYmU2MGQ3YjVhMTlcIn0ifQ.XSXkqVFqiloNyK4vh_BszAM2BsFHsp-yvLvK1tXapcNLFA-46BUDUSbcnyXHVsXcUBUjMKv6Leo6jkrKmjgT3Q"
  val assessmentStudentSession = "7c3a5916-3921-4cc0-99b3-cbe60d7b5a19"
  val assessmentStudentCourse = "5704"
  val forumAuth = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJ1c2VybmFtZVwiOlwiYmh1dmFuLmthcG9vcitzdHVkZW50QHVwZ3JhZC5jb21cIixcInNlc3Npb25JRFwiOlwiJDJlYTViNWVlLWQzOGQtNDA5Ny05YzcxLTZlMmZjNTI5NGNhOVwifSJ9.QqASYc4k_EffT9wIC9Kqwg0J4tnLmUkebBEAWW256QjCi5-uHaW3pKC-Vk_0rAoMouirCdHYspsC84oLT8E8eA"
  val forumSession = "$2ea5b5ee-d38d-4097-9c71-6e2fc5294ca9"
  val forumCourse = "163"
  val studentAuth = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJ1c2VybmFtZVwiOlwiYmh1dmFuLmthcG9vcitzdHVkZW50QHVwZ3JhZC5jb21cIixcInNlc3Npb25JRFwiOlwiJW05Ulh1YjVvbFlDXCJ9In0.8HjeGzOPc3w1L-p7vDhaac4X118VaPaypUJJ_IzVRkcuaEt4in-9llMkxIwB6oW5yXbi8cWnkBnGcorkgD8hPQ"
  val studentSession = "%qEeWu3ffJOi"
  val mercuryAdminAuth = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJzZXNzaW9uSURcIjpcIllRSWRCb0YzTWtcIixcInVzZXJuYW1lXCI6XCJjaGVja21hdGVfc2VydmljZStpbnRlcm5hbGFwcEB1cGdyYWQuY29tXCJ9In0.rdN0SgSzXV_EwAvJS4KKSYGMRqKJqZOf2Tnfxy1JGfNWh6dOqRqh-f6DNiNP7gDvIm4BnNiZll15MD8cxD_wpw"
  val mercuryAdminSession = "YQIdBoF3Mk"

  val headers_staging_central = Map("Content-Type" -> contentType, "auth-token" -> adminAuth, "sessionId" -> adminSession)
  val headers_staging_assessment = Map("auth-token" -> assessmentAuth, "sessionId" -> assessmentSession, "courseId" -> assessmentCourse)
  val headers_staging_assessment_student = Map("auth-token" -> assessmentStudentAuth, "sessionId" -> assessmentStudentSession, "courseId" -> assessmentStudentCourse)
  val headers_staging_forum = Map("auth-token" -> forumAuth, "sessionId" -> forumSession, "courseId" -> forumCourse)
  val headers_staging_student_central = Map("Content-Type" -> contentType, "auth-token" -> studentAuth, "sessionId" -> studentSession)
  val headers_mercury_admin = Map("Content-Type" -> contentType, "auth-token" -> mercuryAdminAuth, "sessionId" -> mercuryAdminSession)
  val headers_mercury_assessment_app_token = Map("app-token" -> "15f3b7ab-998d-40c5-9151-3882f192c462")


  val signup_json = "data/signup_data.json"
  val enroll_json = "data/enroll_data.json"
  val application_json = "data/application_data.json"
  val login_json = "data/login_data.json"
  val create_new_quiz = "data/create_quiz.json"
  val create_new_question = "data/create_question.json"

  val feeder = Iterator.continually(Map("email" -> (Random.alphanumeric.take(25).mkString + "@upgrad.com")))
  val program_feeder = Array(Map("program" -> "data-analytics"), Map("program" -> "ml-and-ai"), Map("program" -> "entrepreneurship"), Map("program" -> "digital-marketing"), Map("program" -> "product-management"), Map("program" -> "software-engineering"), Map("program" -> "sales-marketing"), Map("program" -> "ds-and-ml-and-ai"), Map("program" -> "blockchain"), Map("program" -> "ds-pgc-cert")).random
  val cloudfront_page_feeder = Array(Map("page" -> "degree-program-android-dev?device=app"), Map("page" -> "mba-deakin-university-app?device=app"), Map("page" -> "mba-business-analytics-nmims-app?device=app"), Map("page" -> "degree-prog-android-live?device=app"), Map("page" -> "blockchain-certification-pgd-iiitb-app"), Map("page" -> "full-stack-developer-course-pgd-iiitb-app")).random

  val csvFeeder = csv("data/user_details.csv").random
  val userWithNoAppFeeder = csv("data/user_with_noapp.csv").random
  val paidActiveUser = csv("data/learn_paid_student.csv").random
  val populateMProgress = csv("data/populate_module_progress.csv")
  val quiz_question_mapping = csv("data/quiz_question_mapping.csv")
  val grader_auth_quiz = csv("data/grader_auth_quiz_question.csv")
  val userCourseModule = csv("data/user_module_course_session.csv")
  val userElective = csv("data/user_elective.csv")
  val forumContentQuestions = csv("data/content_question.csv")
  val forumQuestionAnswers = csv("data/question_answer_forum.csv")

  val pause_before_start = 5
  val number_of_requests_at_once = 2
  val ramp_up_users = 10
  val ramp_up_time = 5
  val constant_users_per_sec = 20
  val ramp_up_users_per_second = 20
}
