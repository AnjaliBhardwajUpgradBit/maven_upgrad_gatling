package centralapi.authapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import loadapi.Base._

object PpandTc {
  val pp_and_tc = feed(paidActiveUser).exec(http("User checks privacy policy")
    .get("/apis/v2/consent/verify?type=PRIVACY_POLICY,TERMS_AND_CONDITIONS&platform=LEARN")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.is(200)))
}
