package centralapi.authapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import scala.util.Random
import loadapi.Base._

object Signup {
  val signup = feed(feeder).exec(http("New user sign's up")
    .post("/apis/v2/signup")
    .headers(headers_staging_central)
    .body(StringBody("""{"email": "${email}", "firstname": "Bhuvan", "lastname": "Kapoor", "phoneNumber": "9999999999", "password": "password" }""")).asJson
    .check(status.in(201))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of signup api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
