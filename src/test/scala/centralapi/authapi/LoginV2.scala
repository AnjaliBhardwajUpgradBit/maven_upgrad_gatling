package centralapi.authapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import loadapi.Base._

object LoginV2 {
  val login_v2 = feed(csvFeeder).exec(http("User logs in using v2")
    .post("/apis/v2/login")
    .headers(headers_staging_central)
    .body(StringBody("""{"username": "${email}", "password": "upgrad123"}""")).asJson
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of login V2 api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
