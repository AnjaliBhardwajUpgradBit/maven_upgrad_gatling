package centralapi.authapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import loadapi.Base._

object CheckEmailExists {
  val check_email_exists = feed(paidActiveUser).exec(http("Check if the user exists")
    .get("/apis/v2/users/exists/by")
    .queryParam("email", "${email}")
    .headers(headers_staging_central)
    .check(status.in(204)))
  //This API has no response body, hence we are not logging it.
}
