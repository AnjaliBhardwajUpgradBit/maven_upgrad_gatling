package centralapi.authapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import loadapi.Base._

object Enroll {
  val enroll = exec(http("Enroll a user")
    .post("/apis/admin/enrollments/single/b2b/byId")
    .headers(headers_staging_central)
    .body(ElFileBody(enroll_json)).asJson
    .check(status.is(200)))
}
