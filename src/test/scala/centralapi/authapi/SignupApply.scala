package centralapi.authapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import scala.util.Random
import loadapi.Base._

object SignupApply {
  val signup_and_apply = feed(feeder).exec(http("Signup and apply")
    .post("/apis/v3/applications/signup-and-apply")
    .headers(headers_staging_central)
    .body(StringBody("""{"firstname": "Load","lastname": "User","email": "${email}","phoneNumber": "+919999999999","city": "Mumbai","state": "Maharashtra","country": "India","leadSource": {"platform": "Desktop Web","platformSection": "Lead Form"},"application": {"firstname": "Load","lastname": "Kapoor","email":  "${email}","phoneNumber": "+919999999999","city": "Mumbai","state": "Maharashtra","country": "India","leadSource": {"platform": "Desktop Web","platformSection": "Lead Form"},"name": "Load Kapoor","major": "B.Tech","yearsOfExp": "","yearOfGraduation": 2015,"finalDegreeMarks": 80,"educations": [{}],"referredBy": "","plan": "","programKey": "ds-pgc-cert","programPackageKey": "ds-pgc-cert","autoShortList": false},"isApplicationComplete": true,"password": "upgrad123","tabIndex": 4}""")).asJson
    .check(status.in(201))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of signup_and_apply api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
