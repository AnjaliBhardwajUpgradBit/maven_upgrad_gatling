package centralapi.authapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import loadapi.Base._

object Authenticate {
  val authenticate = feed(paidActiveUser).exec(http("User gets authenticated")
    .get("/apis/v2/authenticate/")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check(jsonPath("$").saveAs("RESPONSE_DATA")))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500") {
      exec(session => {
        println("Result of authenticate api:")
        println(session("RESPONSE_DATA").as[String])
        session
      })
    }
}
