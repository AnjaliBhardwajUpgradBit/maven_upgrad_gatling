package centralapi.authapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import loadapi.Base._

object LoginV3 {
  val login_v3 = feed(csvFeeder).exec(http("User logs in using V3 api")
    .post("/apis/v3/login")
    .headers(headers_staging_central)
    .body(StringBody("""{"username": "${email}", "password": "upgrad123"}""")).asJson
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of login V3 api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
