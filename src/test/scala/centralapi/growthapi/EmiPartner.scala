package centralapi.growthapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object EmiPartner {
  val emi_partner = exec(http("User gets emi partner details")
    .get("/apis/v3/payment/loan-partners/master-data-science?amountPaying=260000")
    .headers(headers_staging_central)
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of emi partner api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
