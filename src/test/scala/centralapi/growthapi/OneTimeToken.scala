package centralapi.growthapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object OneTimeToken {
  val one_time_token_generate = feed(paidActiveUser).exec(http("User generates the token")
    .get("/apis/v2/user/one-time-token")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA_1" ))
    .check(bodyString.saveAs("Response_of_api")))
    .doIf(session => session("OK").as[String] == "400" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
         exec(session => {
                       println( "Result of /apis/v2/one-time-token api:" )
                       println( session( "RESPONSE_DATA_1" ).as[String] )
                       session})
      }

    val one_time_token_verify =
      exec(http("User verifies the token")
      .post("/apis/v2/user/one-time-token/verify")
      .headers(Map("Content-Type" -> "application/json"))
      .body(StringBody("${Response_of_api}"))
      .check(status.in(204)))
}
