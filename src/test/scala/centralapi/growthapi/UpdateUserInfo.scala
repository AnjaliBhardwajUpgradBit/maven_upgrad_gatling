package centralapi.growthapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object UpdateUserInfo {
  val update_user_info = feed(userWithNoAppFeeder).exec(http("User updates phone number")
    .put("/apis/v2/users/account")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .body(StringBody("""{ "phoneNumber": "999999999" }""")).asJson
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of user account api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
