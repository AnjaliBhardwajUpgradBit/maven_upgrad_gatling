package centralapi.growthapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object CampaignContact {
  val campaign_contact = feed(program_feeder).exec(http("User gets contact number")
    .get("/apis/v2/campaign-contact-number/get?programPackageKey=${program}&utmCampaign=&utmSource=&utmMedium=")
    .headers(headers_staging_central)
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check(jsonPath("$").saveAs("RESPONSE_DATA")))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
       exec( session => {
                      println( "Result of campaign contact api:" )
                      println( session( "RESPONSE_DATA" ).as[String] )
                      session})
       }
}
