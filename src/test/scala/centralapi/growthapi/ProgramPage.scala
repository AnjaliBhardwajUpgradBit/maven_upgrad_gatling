package centralapi.growthapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object ProgramPage {
  val program_page = exec(http("User check an existing program")
    .get("/apis/v2/programs/cohort/data-analytics")
    .headers(headers_staging_central)
    .check(status.is(200)))
}
