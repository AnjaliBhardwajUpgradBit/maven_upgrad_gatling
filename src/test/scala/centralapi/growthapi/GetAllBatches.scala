package centralapi.growthapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetAllBatches {
  val get_all_batches = exec(http("User gets all batches")
    .get("/apis/v2/programpackages/batch/all")
    .headers(headers_staging_central)
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check(jsonPath("$").saveAs("RESPONSE_DATA")))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
       exec( session => {
                      println( "Result of programpackages/batch api:" )
                      println( session( "RESPONSE_DATA" ).as[String] )
                      session})
       }
}
