package centralapi.growthapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object Application {
  val application = feed(userWithNoAppFeeder).exec(http("User creates a new application")
    .post("/apis/v2/applications")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .body(StringBody("""{
      "application": {
                        "firstname":   "Load",
                        "lastname":    "test",
                        "email":       "${email}",
                        "phoneNumber": "9999999999",
                        "dateOfBirth": "29/11/1992",
                        "gender":      "male",
                        "address1":    "line1",
                        "address2":    "line2",
                        "city":        "Mumbai",
                        "country":     "India",
                        "state":       "Maharashtra",
                        "courseId":    "275",
                        "programId":   "5",
                        "batchName":   "JAN 2019"
                     }
    }""")).asJson
    .check(status.in(201))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
       exec( session => {
                       println( "Result of create_application api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
                     }
}
