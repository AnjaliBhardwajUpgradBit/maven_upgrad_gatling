package centralapi.growthapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object DropLead {
  val drop_lead = feed(feeder).feed(program_feeder).exec(http("User drops the lead")
    .post("/apis/v3/drop-lead")
    .headers(headers_staging_central)
    .body(StringBody("""{ "firstname": "FirstName", "lastname": "LastName", "email": "${email}", "phoneNumber": "9999999999", "course": "${program}", "courseList": ["${program}"], "emailTemplate": "templateName", "sendWelcomeMail": false, "city": "Mumbai", "affiliateSource": "aff_id=1&sub_aff_id=12", "clickIdentifier": "gclid" , "leadSource" : { "platform" : "", "platformSection" : "", "platformCounsellor" : "" } } """)).asJson
    .check(status.in(201))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
         exec(session => {
                       println( "Result of drop_lead api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
      }
  val drop_lead_cycle = scenario("User drops lead").exec(drop_lead)
}
