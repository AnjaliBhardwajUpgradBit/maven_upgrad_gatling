package centralapi.learnapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetPermission {
  val get_permission = feed(paidActiveUser).exec(http("User checks permissions")
    .get("/apis/v2/enrollments/permissions/${course_id}")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.is(200)))
}
