package centralapi.learnapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetV2EnrollmentStudent {
  val get_v2_enrollment_student = exec(http("get enrollment of a student v2")
    .get("/apis/v2/enrollments")
    .headers(headers_staging_student_central)
    .check(status.is(200)))
}
