package centralapi.learnapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object ModuleGroup {
  val modulegroup = feed(paidActiveUser).exec(http("User gets all module_group of a course")
    .get("/apis/v2/modulegroups?courseId=${course_id}")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of get_all_module_group_by_course api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
