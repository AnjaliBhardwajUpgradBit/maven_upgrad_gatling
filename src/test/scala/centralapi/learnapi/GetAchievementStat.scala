package centralapi.learnapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetAchievementStat {
  val get_achievement_stat = feed(paidActiveUser).exec(http("get achievement stat of a user")
    .get("/apis/v2/badge/learner-achievement-stats/course/${course_id}/user/${user_id}/")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of achievement api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
