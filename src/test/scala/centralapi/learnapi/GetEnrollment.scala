package centralapi.learnapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetEnrollment {
  val get_enrollment = feed(paidActiveUser).exec(http("get enrollment of a user")
    .get("/apis/v3/enrollments?pageSize=10&platform=web-student")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of get enrollment api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
