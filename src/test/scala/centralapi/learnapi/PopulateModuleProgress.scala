package centralapi.learnapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object PopulateModuleProgress {
  val populate_module_progress = feed(populateMProgress).exec(http("Populating module progress")
    .put("/apis/v2/progress/${user_id}/populateprogress/module/${module_id}")
    .headers(headers_staging_central)
    .check(status.is(204)))
}
