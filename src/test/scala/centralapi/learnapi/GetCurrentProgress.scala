package centralapi.learnapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetCurrentProgress {

  val get_segment_page = feed(paidActiveUser).exec(http("User gets the segment page for progress")
    .get("/apis/v2/progress/segment/${segment_id}")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA_1" )))
    .doIf(session => session("OK").as[String] == "400" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
         exec(session => {
                       println( "Result of /apis/v2/progress/segment/${segment_id}:" )
                       println( session( "RESPONSE_DATA_1" ).as[String] )
                       session})
      }

  val get_current_progress = feed(paidActiveUser).exec(http("get current progress of a user")
    .get("/apis/v2/progress/current?courseId=${course_id}")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of get_current_progress api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
