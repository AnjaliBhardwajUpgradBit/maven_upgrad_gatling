package centralapi.learnapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object ElectiveConfiguration {
  val elective_configuration = feed(userElective).exec(http("User checks all elective congifuration")
    .get("/apis/v2/electiveconfigurations/course/${course_id}/all")
    //admin only calls this API
    .headers(headers_mercury_admin)
    .check(status.is(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of achievement api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
