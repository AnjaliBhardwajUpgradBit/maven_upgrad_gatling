package centralapi.learnapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object Course {
  val course = exec(http("User checks a course")
    .get("/apis/v2/courses/275")
    .headers(headers_staging_central)
    .check(status.is(200)))
}
