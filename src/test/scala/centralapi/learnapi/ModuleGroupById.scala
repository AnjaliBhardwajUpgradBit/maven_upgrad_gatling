package centralapi.learnapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object ModuleGroupById {
  val modulegroup_byid = feed(paidActiveUser).exec(http("User gets module_group by id")
    .get("/apis/v2/modulegroups/${module_group_id}?courseId=${course_id}&withNumber=false&complete=true")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of get_module_group_by_id api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
