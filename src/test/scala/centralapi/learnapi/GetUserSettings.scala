package centralapi.learnapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetUserSettings {
  val get_user_settings = feed(paidActiveUser).exec(http("User checks user setting")
    .get("/apis/v2/usersettings")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of user_settings api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
