package centralapi.learnapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetV2EnrollmentAdmin {
  val get_v2_enrollment_admin = exec(http("get enrollment of a admin v2")
    .get("/apis/v2/enrollments")
    .headers(headers_staging_central)
    .check(status.is(200)))
}
