package centralapi.learnapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetMultipleModuleProgress {
  val get_multiple_module_progress = feed(userCourseModule).exec(http("Get multiple progress for user")
    .get("/apis/v3/progress/modules/${module}?courseId=${course}")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of /apis/v3/progress/modules/${module}?courseId=${course} api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
