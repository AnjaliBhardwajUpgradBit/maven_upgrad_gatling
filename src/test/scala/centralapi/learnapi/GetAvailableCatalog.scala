package centralapi.learnapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object GetAvailableCatalog {
  val get_available_catalog = feed(paidActiveUser).exec(http("get list of avaialable catalog")
    .get("/apis/v3/courses/availableCatalog?platform=web-student&pageSize=10")
    .headers(Map("Content-Type" -> "application/x-www-form-urlencoded", "auth-token" -> "${token}", "sessionId" -> "${last_session_id}"))
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result of available_catalog api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
