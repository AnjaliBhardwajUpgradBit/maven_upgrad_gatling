package cloudfrontapi

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import loadapi.Base._

object DegreePage {
  val degree_page = feed(cloudfront_page_feeder).exec(http("User gets the degrees page on cloudfront")
    .get("/api/pages/${page}")
    .headers(headers_staging_central)
    .check(status.in(200))
    .check(status.saveAs("OK"))
    .check( jsonPath( "$" ).saveAs( "RESPONSE_DATA" )))
    .doIf(session => session("OK").as[String] == "409" || session("OK").as[String] == "404" || session("OK").as[String] == "401" || session("OK").as[String] == "500")
       {
    exec(session => {
                       println( "Result degree page home cloudfront api:" )
                       println( session( "RESPONSE_DATA" ).as[String] )
                       session})
}
}
